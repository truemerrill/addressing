from TASK1Pulses import *
import pickle
import numpy
import matplotlib
import matplotlib.pyplot as plt

filename = "TAN2ContourPlot.pkl"

def angle(upsilon, alpha):
    """Angle function for the AN2 / TAN2 sequences"""
    Ang = numpy.zeros( (len(upsilon), len(alpha)) )
    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 4*alpha[a] < upsilon[u]:
                Ang[u,a] = numpy.NaN
            else:
                A = TAN2( upsilon[u], alpha[a] )
                Ang[u,a] = A.Angle()

    return Ang

def time(upsilon, alpha):
    """Time function for the AN2 / TAN2 sequences"""
    T = numpy.zeros( (len(upsilon), len(alpha)) )
    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 4*alpha[a] < upsilon[u]:
                T[u,a] = numpy.NaN
            else:
                A = TAN2( upsilon[u], alpha[a] )
                T[u,a] = A.Time()

    return T

def error(upsilon, alpha):
    """Error function for the AN2 / TAN2 sequences"""
    E = numpy.zeros( (len(upsilon), len(alpha)) )
    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 4*alpha[a] < upsilon[u]:
                E[u,a] = numpy.NaN
            else:
                #phiAN2 = arccos( - upsilon[u] / (4*alpha[a]) )
                #E[u,a] = alpha[a]**6 / 72.0 * (5 + 4*cos(2*phiAN2)) * (sin(2*phiAN2)**2 )
                A = TAN2( upsilon[u], alpha[a] )
                E[u,a] = A.Error

    return E

def measure(upsilon, alpha):
    """Measure function for the AN2 / TAN2 sequences"""
    Ang = numpy.zeros( (len(upsilon), len(alpha)) )
    T = numpy.zeros( (len(upsilon), len(alpha)) )
    E = numpy.zeros( (len(upsilon), len(alpha)) )

    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 4*alpha[a] < upsilon[u]:
                Ang[u,a] = numpy.NaN
                T[u,a] = numpy.NaN
                E[u,a] = numpy.NaN
            else:
                A = TAN2( upsilon[u], alpha[a] )
                Ang[u,a] = A.Angle()
                T[u,a] = A.Time()
                E[u,a] = A.Error


    return Ang, T, E


def ContourCalc():
    """

    """
    # Make a grid of points to sample the net rotation angle, error,
    # and sequnce time for the contour data.
    points = 100.0
    upsilon = numpy.arange( 1e-6, 2*pi - 1e-6, 2*pi/points )
    alpha = numpy.arange( 1e-6, pi - 1e-6, pi/points )
    Upsilon, Alpha = numpy.meshgrid(upsilon, alpha)

    # Collect contour data
    Angle, Time, Error = measure(upsilon, alpha)
    AngleContours = numpy.arange(pi/4, 2*pi, pi/4)

    # Compute matching time contours and Error contours
    TimeContours = 0 * AngleContours
    ErrorContours = 0 * AngleContours
    PointsTimeContours = {}
    PointsErrorContours = {}
    PointsTimeContours["Upsilon"] = 0 * AngleContours
    PointsTimeContours["Alpha"] = 0 * AngleContours
    PointsErrorContours["Upsilon"] = 0 * AngleContours
    PointsErrorContours["Alpha"] = 0 * AngleContours

    for index in range( len(TimeContours) ):
        MinT = MinTimeTAN2( AngleContours[index] )
        TimeContours[index] = MinT.Time()
        PointsTimeContours["Upsilon"][index] = MinT.Upsilon
        PointsTimeContours["Alpha"][index] = MinT.Alpha

        MinE = MinErrorTAN2( AngleContours[index] )
        ErrorContours[index] = MinE.Error
        PointsErrorContours["Upsilon"][index] = MinE.Upsilon
        PointsErrorContours["Alpha"][index] = MinE.Alpha

    # Trace out the minimal time and minimal error paths.  Sample at a
    # higher density.
    angle = numpy.arange(1e-6, 2*pi - 1e-6, 2*pi/1000.0).tolist()
    angle.append(2*pi - 1e-6)
    angle = numpy.array( angle )

    # Compute optimal time trajectory
    MinTime = {}
    MinTime['Angle'] = angle
    MinTime['Upsilon'] = 0 * angle
    MinTime['Alpha'] = 0 * angle
    MinTime['Time'] = 0 * angle
    MinTime['Error'] = 0 * angle

    for index in range( len(angle) ):
        try:
            sol = MinTimeTAN2( angle[index] )
            MinTime['Upsilon'][index] = sol.Upsilon
            MinTime['Alpha'][index] = sol.Alpha
            MinTime['Time'][index] = sol.Time()
            MinTime['Error'][index] = sol.Error
        except:
            MinTime['Upsilon'][index] = numpy.NaN
            MinTime['Alpha'][index] = numpy.NaN
            MinTime['Time'][index] = numpy.NaN
            MinTime['Error'][index] = numpy.NaN

    # Compute optimal error trajectory
    MinError = {}
    MinError['Angle'] = angle
    MinError['Upsilon'] = 0 * angle
    MinError['Alpha'] = 0 * angle
    MinError['Time'] = 0 * angle
    MinError['Error'] = 0 * angle

    for index in range( len(angle) ):
        try:
            sol = MinErrorTAN2( angle[index] )
            MinError['Upsilon'][index] = sol.Upsilon
            MinError['Alpha'][index] = sol.Alpha
            MinError['Time'][index] = sol.Time()
            MinError['Error'][index] = sol.Error
        except:
            MinError['Upsilon'][index] = numpy.NaN
            MinError['Alpha'][index] = numpy.NaN
            MinError['Time'][index] = numpy.NaN
            MinError['Error'][index] = numpy.NaN

    # Save data into a pickle
    f = open(filename,"w")
    data = (
            Upsilon,
            Alpha,
            Angle,
            Time,
            Error,
            MinTime,
            MinError,
            AngleContours,
            TimeContours,
            PointsTimeContours,
            ErrorContours,
            PointsErrorContours
            )

    pickle.dump(data, f)
    f.close()
    return data

def ContourLoad():
    """

    """
    f = open(filename,"r")
    data = pickle.load(f)
    f.close()
    return data

def ContourPlot(data):
    """

    """
    # Use TeX for labels
    matplotlib.rc('text', usetex=True)

    # Ungroup data tuple
    Upsilon, Alpha, Angle, Time, Error, MinTime, MinError, AngleContours,\
             TimeContours, PointsTimeContours,\
             ErrorContours, PointsErrorContours = data

    # Create figure
    figsize = (5,5)
    figure = plt.figure(figsize=figsize)
    ContourAngle = plt.contour(Upsilon, Alpha, numpy.transpose(Angle), AngleContours,
                               colors = 'k',
                               alpha = 0.6,
                               linestyles = 'solid',
                               linewidth = 2
                               )

    ContourTime = plt.contour(Upsilon, Alpha, numpy.transpose(Time), TimeContours,
                              colors = 'r',
                              alpha = 0.6,
                              linestyles = 'solid',
                              linewidth = 1.5
                              )

    FineTimeContours = numpy.arange(pi/2, 6*pi, pi/4)
    ContourFineTime = plt.contour(Upsilon, Alpha, numpy.transpose(Time), FineTimeContours,
                              colors = 'k',
                              alpha = 0.4,
                              linestyles = 'solid',
                              linewidth = 1.5
                              )

    # Create my own colormap
    ## cdict = { 'red':   ((0.0, 1.0, 1.0), (1.0, 0.0, 0.0)), \
    ##           'green': ((0.0, 1.0, 1.0), (1.0, 0.0, 0.0)), \
    ##           'blue':  ((0.0, 1.0, 1.0), (1.0, 0.0, 0.0))  }
    ## plt.register_cmap(name='custom', data = cdict)

    ## ImageTime = plt.imshow(Time.transpose(),
    ##                        interpolation='Bilinear',
    ##                        origin=(0,0),
    ##                        extent=(0, 2*pi, 0, 2*pi),
    ##                        alpha = 0.9,
    ##                        cmap='custom'
    ##                        )

    #ContourError = plt.contour(Alpha, Upsilon, Error, ErrorContours,
    #                           colors = 'g',
    #                           alpha = 0.6,
    #                           linestyles = 'dashed'
    #                           )

    TraceN2 = plt.plot( numpy.array([0, 2*pi]), numpy.array([pi-0.01, pi-0.01]),
                         color = 'b',
                         linestyle = '-',
                         linewidth = 2
                         )

    TraceMinTime =  plt.plot( MinTime["Upsilon"], MinTime["Alpha"],
                              color = "r",
                              linestyle = "-",
                              linewidth = 2
                              )

    PointsMinTime = plt.plot( PointsTimeContours["Upsilon"], PointsTimeContours["Alpha"],
                              color = "r",
                              linestyle = "None",
                              marker = "o",
                              linewidth = 2
                              )

    TraceMinError = plt.plot( MinError["Upsilon"], MinError["Alpha"],
                              color = "g",
                              linestyle = "-",
                              linewidth = 2
                              )

    PointsMinTime = plt.plot( PointsErrorContours["Upsilon"], PointsErrorContours["Alpha"],
                              color = "g",
                              linestyle = "None",
                              marker = "o",
                              linewidth = 2
                              )

    plt.xlabel(r'$\theta_1$')
    plt.ylabel(r'$\theta_2$')
    plt.xticks(
        (0, pi/2, pi, 3*pi/2, 2*pi),
        (r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$')
        )
    plt.yticks(
        (0, pi/4, pi/2, 3*pi/4, pi),
        (r'$0$', r'$\pi/4$', r'$\pi/2$', r'$3\pi/4$', r'$\pi$')
        )
    plt.axis([0, 2*pi+0.02, 0, pi])
    figure.show()

    # Create figure
    figsize = (5,2.5)
    figure = plt.figure(figsize=figsize)

    TimeN2 = plt.plot( numpy.array([0, 2*pi]), numpy.array([4*pi, 6*pi]),
                        color = 'b',
                        linestyle = '-',
                        linewidth = 2
                        )

    TimeMinError = plt.plot( MinError["Angle"], MinError["Time"],
                             color = 'g',
                             linestyle = '-',
                             linewidth = 2
                             )

    TimeMinTime = plt.plot( MinTime["Angle"], MinTime["Time"],
                            color = 'r',
                            linestyle = '-',
                            linewidth = 2
                            )

    plt.axis([0, 2*pi, 0, 6*pi])
    plt.xlabel(r'$\vartheta$')
    #plt.ylabel(r'Sequence time')
    plt.xticks(
        (0, pi/2, pi, 3*pi/2, 2*pi),
        (r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$')
        )
    plt.yticks(
        (0, pi, 2*pi, 3*pi, 4*pi, 5*pi, 6*pi),
        (r'$0$', r'', r'$2\pi$', r'', r'$4\pi$', r'', r'$6\pi$')
        )
    figure.show()

    # Create figure
    figsize = (5,2.5)
    figure = plt.figure(figsize=figsize)

    N2Error = error( MinTime["Angle"], [pi] )
    ErrorN2 = plt.plot( MinTime["Angle"], N2Error,
                        color = 'b',
                        linestyle = '-',
                        linewidth = 2
                        )

    ErrorMinError = plt.plot( MinError["Angle"], MinError["Error"],
                             color = 'g',
                             linestyle = '-',
                             linewidth = 2
                             )

    ErrorMinTime = plt.plot( MinTime["Angle"], MinTime["Error"],
                            color = 'r',
                            linestyle = '-',
                            linewidth = 2
                            )

    MaxError = float( error([2*pi],[pi]) )
    plt.axis([0, 2*pi, 0, MaxError])
    plt.xlabel(r'$\vartheta$')
    #plt.ylabel(r'Infidelity/$\epsilon^4$')
    plt.xticks(
        (0, pi/2, pi, 3*pi/2, 2*pi),
        (r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$')
        )
    #plt.yticks(
    #    (0, pi, 2*pi, 3*pi, 4*pi, 5*pi, 6*pi),
    #    (r'$0$', r'', r'$2\pi$', r'', r'$4\pi$', r'', r'$6\pi$')
    #    )

    figure.show()

if __name__ == "__main__":

    # Try to load pickle if it exists
    try:
        data = ContourLoad()
        print "Loaded data from %s" %(filename)

    except:
        print "Calculating ... "
        data = ContourCalc()


    print "Plotting ..."
    ContourPlot(data)
