from TASK1Pulses import *

# Make tables of optimal trajectories

HEADER = r"Sequence: %s" "\n" +\
         r"U_T = R(%s,0)" "\n" +\
         r"================" "\n"

FOOTER = r"================" "\n"

def table(sequence, name, label):

    string = HEADER %(name, label)
    for pulse in sequence:

        entry = r"{%.4f, %.4f}," %(pulse[0], pulse[1])
        string = string + entry + "\n"

    string = string + FOOTER

    return string


if __name__ == "__main__":
    # Print out tables

    angles = [pi/4, 2*pi/4, 3*pi/4, 4*pi/4, 5*pi/4, 6*pi/4, 7*pi/4, 8*pi/4]
    labels = [
        r"\pi/4",
        r"\pi/2",
        r"3 \pi/4",
        r"\pi",
        r"5 \pi/4",
        r"3 \pi/2",
        r"7 \pi/4",
        r"2 \pi"
        ]
    names = [
        r"MinTimeTASK1",
        r"MinErrorTASK1",
        r"MinTimeTAN2",
        r"MinErrorTAN2"
        ]
    
    Sequences = [MinTimeTASK1, MinErrorTASK1, MinTimeTAN2, MinErrorTAN2]
    String = ""

    for seqindex in range(len(Sequences)):
        seq = Sequences[seqindex]
        name = names[seqindex]

        for index in range(len(angles)):
            angle = angles[index]
            label = labels[index]
            
            print "%s, %s" %(str(seq),label)
            A = seq(angle)
            A.Clean()
            Table = table(A, name, label)

            String = String + Table + "\n\n\n"

    print "RESULTS:\n\n\n"
    print String
