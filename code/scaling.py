from TASK1Pulses import *
import matplotlib
import matplotlib.pyplot as plt
import fileinput


def Scale(epsilon, sequence):
    """

    """
    rescaled = sequence.copy()
    for pulse in rescaled:
        pulse[0] = epsilon * pulse[0]

    return rescaled


def Scaling(sequence, prefix = None, plot = True):
    """

    """
    epsilon = numpy.arange(0, 1.2, 0.01)
    population = numpy.zeros( epsilon.shape )

    for index in range(len(epsilon)):
        ep = epsilon[index]
        rescaled = Scale(ep, sequence)
        U = rescaled.Operator()
        matrix_element = U[0,1]
        population[index] = numpy.real(U[0,1] * numpy.conj(U[0,1]))

    # Save as a text wave
    if prefix == None:
        numpy.savetxt("population.txt", population)
        numpy.savetxt("epsilon.txt", epsilon)
    else:
        numpy.savetxt(prefix + "_population.txt", population)
        numpy.savetxt(prefix + "_epsilon.txt", epsilon)
        
        # Add headers to the text files
        with open(prefix + "_population.txt", "r+") as f:
            data = f.read()
            f.seek(0)
            f.write(prefix + "_population\n" + data)
            f.close()

        with open(prefix + "_epsilon.txt", "r+") as f:
            data = f.read()
            f.seek(0)
            f.write(prefix + "_epsilon\n" + data)
            f.close()

    # Plot
    if plot:
        plt.plot(epsilon, population)
        plt.show()
    

def WaistScaling():
    """

    """
    x = numpy.arange(0.0, 3.5, 0.01)
    sequences = [
        Sequence([[pi,0]]),
        SK1(pi),
        NB1(pi),
        MinErrorTASK1(pi),
        MinTimeTASK1(pi),
        ]

    plotstyle = [
        "k",
        "r--",
        "g",
        "b",
        "c-."
        ]

    for j in range(len(sequences)):
        sequence = sequences[j]
        infidelity = numpy.zeros( x.shape )

        for index in range(len(x)):
            epsilon = exp( - (x[index]**2) )
            rescaled = Scale(epsilon, sequence)

            U = rescaled.Operator()
            f = numpy.abs( numpy.trace(U) ) / 2.0
            infidelity[index] = 1 - f

        plt.semilogy(x, infidelity, plotstyle[j])

    plt.axis([0, 3.5, 1e-15, 1])
    plt.yticks([1e-15, 1e-10, 1e-5, 1])
    plt.show()


def Waist(sequence):
    """

    """
    x = numpy.arange(-2.5, 2.5, 0.01)
    population = numpy.zeros( x.shape )

    # Calibrated effective epsilon at beam center
    E0 = 2.9605 / pi

    for index in range(len(x)):
        epsilon = E0 * exp( - (x[index]**2) )
        rescaled = Scale(epsilon, sequence)

        U = rescaled.Operator()
        matrix_element = U[0,1]
        population[index] = numpy.real(U[0,1] * numpy.conj(U[0,1]))

    # Save as a text wave
    numpy.savetxt("population.txt", population)
    numpy.savetxt("position.txt", x)

    # Plot
    plt.plot(x, population)
    plt.show()



# SK1 and NB1
def SK1(theta):
    """

    """
    phi = arccos(-theta/(4*pi))
    return Sequence([
        [theta, 0],
        [2*pi, phi],
        [2*pi,-phi]
        ])

def NB1(theta):
    """

    """
    phi = arccos(-theta/(4*pi))
    return Sequence([
        [theta, 0],
        [pi, phi],
        [2*pi, - phi],
        [pi, phi]
        ])

if __name__ == "__main__":
    # Main program
    
    angles = [pi/4, pi/2, 3*pi/4, pi, 5*pi/4, 3*pi/2, 7*pi/4, 2*pi]
    prefixes = [
        "0.25_pi",
        "0.50_pi",
        "0.75_pi",
        "1.00_pi",
        "1.25_pi",
        "1.50_pi",
        "1.75_pi",
        "2.00_pi"
        ]

    sequence_names = [
        "_Simple",
        "_SK1",
        "_NB1",
        "_TASK1e",
        "_TASK1t"
        ]
    
    for index in range(len(angles)):
        angle = angles[index]
        prefix = prefixes[index]
        
        # List of sequences to calculate
        sequences = [
            Sequence([[angle,0]]),
            SK1(angle),
            NB1(angle),
            MinErrorTASK1(angle),
            MinTimeTASK1(angle),
            ]
        
        for sequence_index in range(len(sequences)):
            sequence = sequences[sequence_index]
            sequence_prefix = prefix + sequence_names[sequence_index]

            # Calculate scaling
            Scaling(
                sequence,
                prefix = sequence_prefix,
                plot = False
                )
