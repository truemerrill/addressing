from TASK1Pulses import *
import numpy
from numpy import pi
    
#
# Methods to convert between (upsilon, alpha) parameterization and the
# (lambdaX, lambdaY) parameterization
#
def CalcLambdaX(upsilon, alpha):
    return upsilon / (2*pi)
    
def CalcLambdaY(upsilon, alpha):
    return alpha/pi * sqrt(1./3 - upsilon**2/(12 * alpha**2))
    
def CalcUpsilon(lambdaX, lambdaY):
    return 2*pi * lambdaX
    
def CalcAlpha(lambdaX, lambdaY):
    return pi * sqrt(lambdaX**2 + 3 * lambdaY**2)
       

#
# Template for the Latex file
#        
_file_contents_ = r"""
\documentclass{article}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage{fullpage}

\begin{document}

\begin{landscape}
%s
\end{landscape}

\end{document}
"""

#
# Template for the table
#
_table_ = r"""
\begin{table}
\begin{tabular}{%s}
%s
\end{tabular}
\caption{
    This is a caption that an intelligent person should write instead of a computer program.
}
\end{table}
"""
        
if __name__ == "__main__":
    
    # Make a list of target angles and sequences to test
    TargetAngles = numpy.arange(pi/4, 2*pi + 0.01, pi/4)
    Sequences = [MinTimeTASK1, MinErrorTASK1]
    SequenceLabels = [r"TASK1 ($T_\text{min}$)", r"TASK1 ($E_\text{min}$)"]
    
    # Start the table header and labels
    TableLabels = [
        r"Sequence Family",
        r"Net Rotation",
        r"$\lambda_x$",
        r"$\lambda_y$",
        r"$r_1$",
        r"$r_2$",
        r"$r_3$",
        r"$r_4$",
        r"$r_5$",
        r"Pulse area",
        r"Infidelity/$\epsilon_j^4$"
        ]
    TableSpec = r"c " * len(TableLabels)
    TableRow = r"%s & " * (len(TableLabels)-1) + r"%s \\" + "\n"
    
    TableHeader = r""
    for Label in TableLabels[:-1]:
        TableHeader += Label + " & "
    TableHeader += TableLabels[-1] + r"\\" + "\n"
    
    # Loop over each sequence
    TableContents = r"\hline \\[-0.7 em]" + "\n" + TableHeader + r"\\[-0.7 em] \hline \\[-0.5 em]" + "\n"
    for SequenceIndex in range(len(Sequences)):
        Seq = Sequences[SequenceIndex]
        SeqLabel = SequenceLabels[SequenceIndex]
        
        # Loop over target angles
        for Angle in TargetAngles:
            TargetSeq = Seq(Angle)
            TargetSeq.Clean()
            
            # Extract data from TargetSeq
            NetRotation = TargetSeq.Angle()
            LambdaX = CalcLambdaX(TargetSeq.Upsilon, TargetSeq.Alpha)
            LambdaY = CalcLambdaY(TargetSeq.Upsilon, TargetSeq.Alpha)
            PulseArea = TargetSeq.Time()
            Infidelity = TargetSeq.Error[0]
                
            # Build up a substitution tuple for string substitution
            (Substitution1, Substitution2) = ([], [])
            for Item in [SeqLabel, NetRotation, LambdaX, LambdaY]:
                if Item.__class__ == str:
                    Substitution1.append(
                        r"\multirow{2}{*}{%s}" %(Item)
                        )
                else:
                    Substitution1.append(
                        r"\multirow{2}{*}{%.4f}" %(Item)
                        )
                Substitution2.append(r"")
                
            # Pulse sequence angles and phases
            for Pulse in TargetSeq:
                Substitution1.append(
                    r"%.4f" %(Pulse[0])
                    )
                Substitution2.append(
                    r"%.4f" %(Pulse[1])
                    )
                    
            # Costs
            for Item in [PulseArea, Infidelity]:
                Substitution1.append(
                    r"\multirow{2}{*}{%.4f}" %(Item)
                    )
                Substitution2.append(r"")    
            
            # Substitute each row and add to table contents
            TableContents += TableRow %tuple(Substitution1) + r"[-0.1 em]"
            TableContents += TableRow %tuple(Substitution2) + r"[0.25 em]"
            
            # Set SeqLabel to a null string, to prevent repeating the label
            SeqLabel = r""
            
    # Add components of the table together and write to a file
    TableContents += r"\hline"
    Table = _table_ %(TableSpec, TableContents)
    FileContents = _file_contents_ %(Table)
    
    f = open("output.tex", "w")
    f.write(FileContents)
    f.close()    
    
