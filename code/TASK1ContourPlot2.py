from TASK1Pulses import *
import pickle
import numpy
import matplotlib
import matplotlib.pyplot as plt

filename = "TASK1ContourPlot2.pkl"

def angle(upsilon, alpha):
    """Time function for the ASK1 / TASK1 sequences"""
    Ang = numpy.zeros( (len(upsilon), len(alpha)) )
    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 2*alpha[a] < upsilon[u]:
                Ang[u,a] = numpy.NaN
            else:
                A = TASK1( upsilon[u], alpha[a] )
                Ang[u,a] = A.Angle()

    return Ang

def time(upsilon, alpha):
    """Time function for the ASK1 / TASK1 sequences"""
    T = numpy.zeros( (len(upsilon), len(alpha)) )
    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 2*alpha[a] < upsilon[u]:
                T[u,a] = numpy.NaN
            else:
                A = TASK1( upsilon[u], alpha[a] )
                T[u,a] = A.Time()

    return T

def error(upsilon, alpha):
    """Time function for the ASK1 / TASK1 sequences"""
    E = numpy.zeros( (len(upsilon), len(alpha)) )
    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 2*alpha[a] < upsilon[u]:
                E[u,a] = numpy.NaN
            else:
                phiASK1 = arccos( - upsilon[u] / (2*alpha[a]) )
                E[u,a] = alpha[a]**4 / 32 * ( sin(2*phiASK1)**2 )


    return E

def measure(upsilon, alpha):
    """Time function for the ASK1 / TASK1 sequences"""
    Ang = numpy.zeros( (len(upsilon), len(alpha)) )
    T = numpy.zeros( (len(upsilon), len(alpha)) )
    E = numpy.zeros( (len(upsilon), len(alpha)) )

    for u in range(len(upsilon)):
        for a in range(len(alpha)):
            if 2*alpha[a] < upsilon[u]:
                Ang[u,a] = numpy.NaN
                T[u,a] = numpy.NaN
                E[u,a] = numpy.NaN
            else:
                A = TASK1( upsilon[u], alpha[a] )
                Ang[u,a] = A.Angle()
                T[u,a] = A.Time()
                E[u,a] = A.Error


    return Ang, T, E
    
def measure2(lambdaX, lambdaY):
    Ang = numpy.zeros( (len(lambdaX), len(lambdaY)) )
    T = numpy.zeros( (len(lambdaX), len(lambdaY)) )
    E = numpy.zeros( (len(lambdaX), len(lambdaY)) )
    
    for index0 in range(len(lambdaX)):
        lx = lambdaX[index0]
        for index1 in range(len(lambdaY)):
            ly = lambdaY[index1]
            upsilon = CalcUpsilon(lx,ly)
            alpha = CalcAlpha(lx,ly)
            A = TASK1(upsilon, alpha)
            Ang[index0, index1] = A.Angle()
            T[index0, index1] = A.Time()
            E[index0, index1] = A.Error
            
    return Ang, T, E
    
def CalcLambdaX(upsilon, alpha):
    return upsilon / (2*pi)
    
def CalcLambdaY(upsilon, alpha):
    return alpha/pi * sqrt(1./3 - upsilon**2/(12 * alpha**2))
    
def CalcUpsilon(lambdaX, lambdaY):
    return 2*pi * lambdaX
    
def CalcAlpha(lambdaX, lambdaY):
    return pi * sqrt(lambdaX**2 + 3 * lambdaY**2)

def ContourCalc():
    """

    """
    # Make a grid of points to sample the net rotation angle, error,
    # and sequnce time for the contour data.
    points = 100.0
    lambdaX = numpy.arange( 1e-6, 1 - 1e-6, 1/(points-1) )
    lambdaY = numpy.arange( 1e-6, 1.25 - 1e-6, 1/(points-1) )
    LambdaX, LambdaY = numpy.meshgrid(lambdaX, lambdaY)
    
    
    #upsilon = numpy.arange( 1e-6, 2*pi - 1e-6, 2*pi/points )
    #alpha = numpy.arange( 1e-6, 2*pi - 1e-6, 2*pi/points )
    #Upsilon, Alpha = numpy.meshgrid(upsilon, alpha)

    # Collect contour data
    #Angle, Time, Error = measure(upsilon, alpha)
    Angle, Time, Error = measure2(lambdaX, lambdaY)
    AngleContours = numpy.arange(pi/4, 2*pi, pi/4)

    # Compute matching time contours and Error contours
    TimeContours = 0 * AngleContours
    ErrorContours = 0 * AngleContours
    PointsTimeContours = {}
    PointsErrorContours = {}
    PointsTimeContours["LambdaX"] = 0 * AngleContours
    PointsTimeContours["LambdaY"] = 0 * AngleContours
    PointsErrorContours["LambdaX"] = 0 * AngleContours
    PointsErrorContours["LambdaY"] = 0 * AngleContours
    
    for index in range( len(TimeContours) ):
        MinT = MinTimeTASK1( AngleContours[index] )
        TimeContours[index] = MinT.Time()
        PointsTimeContours["LambdaX"][index] = CalcLambdaX(MinT.Upsilon, MinT.Alpha)
        PointsTimeContours["LambdaY"][index] = CalcLambdaY(MinT.Upsilon, MinT.Alpha)
        
        MinE = MinErrorTASK1( AngleContours[index] )
        ErrorContours[index] = MinE.Error
        PointsErrorContours["LambdaX"][index] = CalcLambdaX(MinE.Upsilon, MinE.Alpha)
        PointsErrorContours["LambdaY"][index] = CalcLambdaY(MinE.Upsilon, MinE.Alpha)
        
    # Trace out the minimal time and minimal error paths.  Sample at a
    # higher density.
    angle = numpy.arange(1e-6, 2*pi - 1e-6, 2*pi/1000.0).tolist()
    angle.append(2*pi - 1e-6)
    angle = numpy.array( angle )

    # Compute optimal time trajectory
    MinTime = {}
    MinTime['Angle'] = angle
    MinTime['LambdaX'] = 0 * angle
    MinTime['LambdaY'] = 0 * angle
    MinTime['Time'] = 0 * angle
    MinTime['Error'] = 0 * angle

    for index in range( len(angle) ):
        try:
            sol = MinTimeTASK1( angle[index] )
            MinTime['LambdaX'][index] = CalcLambdaX(sol.Upsilon, sol.Alpha)
            MinTime['LambdaY'][index] = CalcLambdaY(sol.Upsilon, sol.Alpha)
            MinTime['Time'][index] = sol.Time()
            MinTime['Error'][index] = sol.Error
        except:
            MinTime['LambdaX'][index] = numpy.NaN
            MinTime['LambdaY'][index] = numpy.NaN
            MinTime['Time'][index] = numpy.NaN
            MinTime['Error'][index] = numpy.NaN

    # Compute optimal error trajectory
    MinError = {}
    MinError['Angle'] = angle
    MinError['LambdaX'] = 0 * angle
    MinError['LambdaY'] = 0 * angle
    MinError['Time'] = 0 * angle
    MinError['Error'] = 0 * angle

    for index in range( len(angle) ):
        try:
            sol = MinErrorTASK1( angle[index] )
            MinError['LambdaX'][index] = CalcLambdaX(sol.Upsilon, sol.Alpha)
            MinError['LambdaY'][index] = CalcLambdaY(sol.Upsilon, sol.Alpha)
            MinError['Time'][index] = sol.Time()
            MinError['Error'][index] = sol.Error
        except:
            MinError['LambdaX'][index] = numpy.NaN
            MinError['LambdaY'][index] = numpy.NaN
            MinError['Time'][index] = numpy.NaN
            MinError['Error'][index] = numpy.NaN

    # Save data into a pickle
    f = open(filename,"w")
    data = (
            LambdaX,
            LambdaY,
            Angle,
            Time,
            Error,
            MinTime,
            MinError,
            AngleContours,
            TimeContours,
            PointsTimeContours,
            ErrorContours,
            PointsErrorContours
            )

    pickle.dump(data, f)
    f.close()
    return data

def ContourLoad():
    """

    """
    f = open(filename,"r")
    data = pickle.load(f)
    f.close()
    return data

def ContourPlot(data):
    """

    """
    # Use TeX for labels
    #matplotlib.rc('text', usetex=True)

    # Ungroup data tuple
    LambdaX, LambdaY, Angle, Time, Error, MinTime, MinError, AngleContours,\
             TimeContours, PointsTimeContours,\
             ErrorContours, PointsErrorContours = data

    # Create figure
    figsize = (5,5)
    figure = plt.figure(figsize=figsize)
    ContourAngle = plt.contour(LambdaX, LambdaY, numpy.transpose(Angle), AngleContours,
                               colors = 'k',
                               alpha = 0.6,
                               linestyles = 'solid',
                               linewidth = 2
                               )

    ContourTime = plt.contour(LambdaX, LambdaY, numpy.transpose(Time), TimeContours,
                              colors = 'r',
                              alpha = 0.6,
                              linestyles = 'solid',
                              linewidth = 1.5
                              )

    FineTimeContours = numpy.arange(pi/2, 7*pi, pi/4)
    ContourFineTime = plt.contour(LambdaX, LambdaY, numpy.transpose(Time), FineTimeContours,
                              colors = 'k',
                              alpha = 0.4,
                              linestyles = 'solid',
                              linewidth = 1.5
                              )

    # Create my own colormap
    ## cdict = { 'red':   ((0.0, 1.0, 1.0), (1.0, 0.0, 0.0)), \
    ##           'green': ((0.0, 1.0, 1.0), (1.0, 0.0, 0.0)), \
    ##           'blue':  ((0.0, 1.0, 1.0), (1.0, 0.0, 0.0))  }
    ## plt.register_cmap(name='custom', data = cdict)

    ## ImageTime = plt.imshow(Time.transpose(),
    ##                        interpolation='Bilinear',
    ##                        origin=(0,0),
    ##                        extent=(0, 2*pi, 0, 2*pi),
    ##                        alpha = 0.9,
    ##                        cmap='custom'
    ##                        )

    #ContourError = plt.contour(Alpha, Upsilon, Error, ErrorContours,
    #                           colors = 'g',
    #                           alpha = 0.6,
    #                           linestyles = 'dashed'
    #                           )
    lx_SK1 = numpy.arange(0, 1, 0.001)
    ly_SK1 = numpy.sqrt((4 - lx_SK1**2 )/3)
    TraceSK1 = plt.plot( lx_SK1, ly_SK1,
                         color = 'b',
                         linestyle = '-',
                         linewidth = 2
                         )

    TraceMinTime =  plt.plot( MinTime["LambdaX"], MinTime["LambdaY"],
                              color = "r",
                              linestyle = "-",
                              linewidth = 2
                              )

    PointsMinTime = plt.plot( PointsTimeContours["LambdaX"], PointsTimeContours["LambdaY"],
                              color = "r",
                              linestyle = "None",
                              marker = "o",
                              linewidth = 2
                              )

    TraceMinError = plt.plot( MinError["LambdaX"], MinError["LambdaY"],
                              color = "g",
                              linestyle = "-",
                              linewidth = 2
                              )

    PointsMinTime = plt.plot( PointsErrorContours["LambdaX"], PointsErrorContours["LambdaY"],
                              color = "g",
                              linestyle = "None",
                              marker = "o",
                              linewidth = 2
                              )

    #plt.xlabel(r'$\lambda_X$')
    #plt.ylabel(r'$\lambda_Y$')
    plt.xticks(
        (0.0, 0.25, 0.5, 0.75, 1.0)
    )
    plt.yticks(
        (0.0, 0.25, 0.5, 0.75, 1.0, 1.25)
    )       
    #    (0, 1./4, 1./2, 3./4, 1.),
    #    (r'$0$', r'$0.25$', r'$0.5$', r'$0.75$', r'$1.0$')
    #    )
    #plt.xticks(
    #    (0, 1./4, 1./2, 3./4, 1.),
    #    (r'$0$', r'$0.25$', r'$0.5$', r'$0.75$', r'$1.0$')
    #    )
    figure.show()

    # Create figure
    figsize = (5,2.5)
    figure = plt.figure(figsize=figsize)

    TimeSK1 = plt.plot( numpy.array([0, 2*pi]), numpy.array([4*pi, 6*pi]),
                        color = 'b',
                        linestyle = '-',
                        linewidth = 2
                        )

    TimeMinError = plt.plot( MinError["Angle"], MinError["Time"],
                             color = 'g',
                             linestyle = '-',
                             linewidth = 2
                             )

    TimeMinTime = plt.plot( MinTime["Angle"], MinTime["Time"],
                            color = 'r',
                            linestyle = '-',
                            linewidth = 2
                            )

    plt.axis([0, 2*pi, 0, 6*pi])
    #plt.xlabel(r'$\vartheta$')
    #plt.ylabel(r'Sequence time')
    #plt.xticks(
    #    (0, pi/2, pi, 3*pi/2, 2*pi),
    #    (r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$')
    #    )
    #plt.yticks(
    #    (0, pi, 2*pi, 3*pi, 4*pi, 5*pi, 6*pi),
    #    (r'$0$', r'', r'$2\pi$', r'', r'$4\pi$', r'', r'$6\pi$')
    #    )
    figure.show()

    # Create figure
    figsize = (5,2.5)
    figure = plt.figure(figsize=figsize)

    SK1Error = error( MinTime["Angle"], [2*pi] )
    ErrorSK1 = plt.plot( MinTime["Angle"], SK1Error,
                        color = 'b',
                        linestyle = '-',
                        linewidth = 2
                        )

    ErrorMinError = plt.plot( MinError["Angle"], MinError["Error"],
                             color = 'g',
                             linestyle = '-',
                             linewidth = 2
                             )

    ErrorMinTime = plt.plot( MinTime["Angle"], MinTime["Error"],
                            color = 'r',
                            linestyle = '-',
                            linewidth = 2
                            )

    MaxError = float( error([2*pi],[2*pi]) )
    plt.axis([0, 2*pi, 0, MaxError])
    #plt.xlabel(r'$\vartheta$')
    #plt.ylabel(r'Infidelity/$\epsilon^4$')
    #plt.xticks(
    #    (0, pi/2, pi, 3*pi/2, 2*pi),
    #    (r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$')
    #    )
    #plt.yticks(
    #    (0, pi, 2*pi, 3*pi, 4*pi, 5*pi, 6*pi),
    #    (r'$0$', r'', r'$2\pi$', r'', r'$4\pi$', r'', r'$6\pi$')
    #    )

    figure.show()

if __name__ == "__main__":

    # Try to load pickle if it exists
    try:
        data = ContourLoad()
        print "Loaded data from %s" %(filename)

    except:
        print "Calculating ... "
        data = ContourCalc()


    print "Plotting ..."
    ContourPlot(data)
