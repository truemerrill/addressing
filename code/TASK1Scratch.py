from TASK1Pulses import *
import numpy

def BB1(theta, phi, epsilon):
    phiBB1 = arccos( - theta / (4*pi))
    sequence = Sequence([
            [theta * (1+epsilon), phi],
            [pi * (1+epsilon), phi + phiBB1],
            [2*pi*(1+epsilon), phi + 3*phiBB1],
            [pi * (1+epsilon), phi + phiBB1]
            ])
    
    return sequence

def SK1(theta, phi, epsilon):
    phiBB1 = arccos( - theta / (4*pi))
    sequence = Sequence([
            [theta * (1+epsilon), phi],
            [2*pi*(1+epsilon), phi + phiBB1],
            [2*pi*(1+epsilon), phi - phiBB1]
            ])
    
    return sequence


def Infidelity( U, Target ):
    F = abs(numpy.trace(U.H * Target))/2.0
    return 1 - F

def FindErrorBound(theta, bound):
    
    Target = R(theta,0)
    
    def Error(x):
        Seq = BB1(theta,0,x[0])
        U = Seq.Operator()
        return Infidelity(U,Target)
    
    def equality_constraint(x):
        err = Error(x)
        return numpy.array([ bound - err ])
    
    bounds = [(0,1)]
    x0 = numpy.array([0.1])
    
    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        Error, x0,
        f_eqcons = equality_constraint,
        bounds = bounds
        )
    
    return optimal

def FindErrorBoundSK1(theta, bound):
    
    Target = R(theta,0)
    
    def Error(x):
        Seq = SK1(theta,0,x[0])
        U = Seq.Operator()
        return Infidelity(U,Target)
    
    def equality_constraint(x):
        err = Error(x)
        return numpy.array([ bound - err ])
    
    bounds = [(0,1)]
    x0 = numpy.array([0.1])
    
    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        Error, x0,
        f_eqcons = equality_constraint,
        bounds = bounds
        )
    
    return optimal
