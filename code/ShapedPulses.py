# ShapedPulses.py
#
# Methods to study shaped pulse sequences
#
# Copyright (C) 2012, 2013 True Merrill
# Georgia Institute of Technology
# School of Chemistry and Biochemistry
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cmath
import numpy
import scipy
import scipy.linalg
import scipy.optimize

# Create local copies of several numpy objects.
from numpy import exp, sqrt, sin, cos, tan, arcsin, arccos, arctan, \
     arctan2, mod, pi

# Pauli Matrices
I = numpy.matrix("1,0;0,1")
X = numpy.matrix("0,1;1,0")
Y = numpy.matrix("0,-1j;1j,0")
Z = numpy.matrix("1,0;0,-1")

# Define rotations
def R(theta, phi):
    """
    This is a test
    """
    if theta < 0:
        theta = -theta
        phi = phi + pi

    Sigma = cos(phi) * X + sin(phi) * Y
    U = cos(theta/2.) * I - 1.j * sin(theta/2.) * Sigma
    return U


# Sequence class
class Sequence(numpy.ndarray):
    """

    """
    def __new__(cls, input_array):
        obj = numpy.asarray(input_array).view(cls)
        return obj

    def __array_finalize__(self, obj):
        return None

    def Flat(self):
        return self.flatten()

    def Unflat(self):
        n = len(self) / 2
        shape = (n,2)
        return self.reshape(shape)

    def Clean(self):
        """
        Method to replace phases and angles with equivalent values
        that are within bounds.  Bounds for each pulse are:

        *   Angle <= 0
        *   0 <= Phase <= 2*pi
        """
        for pulse in self:
            theta = pulse[0]
            phi = pulse[1]

            # Check for negative rotation angles.  If angles are
            # negative, apply a positive rotation about the opposite.
            if theta < 0:
                theta = -theta
                phi = phi + pi

            # Check that phi is in (0, 2pi)
            phi = mod(phi, 2*pi)

            # Update stored values
            pulse[0] = theta
            pulse[1] = phi

    def Close(self):
        """
        Method to add a final pulse that 'closes' the dynamical Lie
        algebra.
        """
        # Find coordinates for closing operation
        (x, y) = (0,0)
        for pulse in self:
            x = x + pulse[0] * cos(pulse[1])
            y = y + pulse[0] * sin(pulse[1])

        theta = sqrt( (-x)**2 + (-y)**2 )
        phi = arctan2( -y, -x )

        # Add new pulse if theta is nonzero
        if not theta == 0.0:
            closer = numpy.array([[theta,phi]])
            return Sequence(numpy.concatenate((self, closer)))

        else:
            return self

    def Operator(self):
        """
        Method to compute the net propagator produced by the pulse
        sequence.
        """
        U = I
        for pulse in self:
            U = R(pulse[0], pulse[1]) * U

        return U

    def Arg(self):
        """
        Method to compute the argument of the net propagator.  Let U
        be the propagator produced by a sequence.  The argument A
        satisfies U = exp(i A), and A has eigenvalues bound between
        -pi and pi.
        """
        Unitary = self.Operator()

        # Diagonalize Unitary
        (u, V) = scipy.linalg.eig(Unitary)
        V = numpy.matrix( V )
        L = V.H * Unitary * V

        # Calculate Arg for each eigenvalue
        A = 0 * L
        for index in range( len(L) ):
            A[index, index] = cmath.phase( L[index,index] )

        return V * A * V.H

    def Decomp(self):
        """
        Method to decompose the net rotation into a rotation axis.
        The Cartesian length is the net rotation angle.
        """
        A = self.Arg()
        vec = numpy.real([
            -numpy.trace( A * X ),
            -numpy.trace( A * Y ),
            -numpy.trace( A * Z )
            ])
        return vec

    def Time(self):
        """
        Method to measure the duration of the total sequence.  This is
        the sum of the magnitudes of the angles.
        """
        time = 0
        for pulse in self:
            time = time + abs( pulse[0] )
        return numpy.array( float(time) )

    def Angle(self):
        """
        Method to measure the net rotation angle of the sequence.
        """
        U = self.Operator()
        Theta = numpy.real(2 * arccos(numpy.trace(I * U)/2))
        return Theta

    def AddPhase(self, phase):
        """
        Method to add a constant phase to all pulses in the sequence.
        """
        for pulse in self:
            pulse[1] = pulse[1] + phase

    def Table(self, disp = True):
        """
        Print (angles,phases) as a row in a pretty LaTeX table
        """
        table = ""

        # Add entry for theta and time
        string = '%.3f & %.3f & ' %(self.Angle(), self.Time())
        table = table + string

        # Add N-1 entries to table, seperating by LaTeX column marker &
        NumPulses = len(self)
        for index in range(NumPulses - 1):
            string = '(%.3f, %.3f) & ' %(self[index][0], self[index][1])
            table = table + string

        # Add final entry
        string = '(%.3f, %.3f) \\ \n' %(self[NumPulses-1][0], self[NumPulses-1][1])
        table = table + string

        if disp:
            print table

        return table


def TimeOptimal(theta, n = 3):
    """
    Finds the time optimal 'closed' pulse sequence of length n that
    produces a net rotation by an angle theta.
    """

    def time(x):
        """Time function"""
        A = Sequence(x).Unflat()
        B = A.Close()
        return B.Time()

    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = Sequence(x).Unflat()
        B = A.Close()
        return numpy.array([ theta - B.Angle() ])

    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 2*pi), (0, 2*pi) ] * (n-1)

    # Initial guess for the solution
    guess = []
    for j in range(n - 1):
        phij = (2*pi / n) * j
        thetaj = (2.0 /n) * sqrt( theta * pi )
        guess.append([thetaj, phij])
    G = Sequence(guess)
    x0 = G.Flat()

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        time, x0,
        f_eqcons = equality_constraint,
        bounds = bounds,
        disp = True
        )

    # Return the optimal sequence
    Optimal = Sequence(optimal).Unflat().Close()
    Optimal.AddPhase( - Optimal[0][1] )
    Optimal.Clean()
    return Optimal


#
# MAIN PROGRAM
#
if __name__ == "__main__":

    import os

    START = '''
from blochlib.quantop import operator
from blochlib.blochlib import *

DENSITY_MATRIX = operator("1,0;0,0")
'''

    def PulseToRotationString(pulse):
        axis = (cos(pulse[1]), sin(pulse[1]), 0)
        String = "rotation((%.6f, %.6f, %.6f), %.3f)" %(axis[0], axis[1], axis[2], pulse[0])
        return String

    for n in range(3, 51):

        FILE_NAME = "optimal_%i_algebra.inp" %(n)
        OUTPUT_NAME = "\"optimal_%i_algebra.svg\"" %(n)
        CONTENTS = START + "OUTPUT_NAME = %s\n" %(OUTPUT_NAME)

        # Compute optimal pulse sequence
        Optimal = TimeOptimal(pi/2, n)
        PULSE_SEQUENCE = "PULSE_SEQUENCE = ["
        for pulse in Optimal:
            String = PulseToRotationString(pulse)
            PULSE_SEQUENCE = PULSE_SEQUENCE + String + ","
        PULSE_SEQUENCE = PULSE_SEQUENCE + "]\n"

        CONTENTS = CONTENTS + PULSE_SEQUENCE
        CONTENTS = CONTENTS + "VIEW = (0,0,1)"

        # Construct file
        f = open(FILE_NAME, 'w')
        f.write(CONTENTS)
        f.close()

        # Run RenderAlgebra
        os.system('render-algebra %s' %(FILE_NAME))
