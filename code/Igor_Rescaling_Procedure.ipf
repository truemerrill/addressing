#pragma rtGlobals=1	// Use modern global access method.


function load_theory_curves(path)
	string path
	
	// Grab a path to load the theory curves
	if (strlen(path) == 0)
		NewPath/o/m = "Choose folder containing theory curves" TheoryPath
		if (V_flag != 0)
			return -1
		endif
		path = "TheoryPath"
	endif
	
	string filename = ""
	variable index = 0
	
	do
		filename = IndexedFile($path, index, ".txt")
		if (strlen(filename) == 0)
			break
		endif
		
		// Load file as a wave
		LoadWave/A/W/G/O/Q/P=$path filename
		index += 1
	while (1)
end

function rescale_theory_curves(angle, theory_prefix, experiment_prefix)
	variable angle
	string theory_prefix
	string experiment_prefix
	
	// Fit the experiment simple rotation curve to calculate
	// a scaling amplitude.  For now I am dropping a possible
	// y-offeset to have the simplist model.
	
	variable /g $(theory_prefix + "Amplitude")
	nvar amplitude = $(theory_prefix + "Amplitude")
	wave rotation = $(experiment_prefix + "rotation")
	wave error = $(experiment_prefix + "x_error_params")
	
	// Hold the rotation angle fixed during fitting
	Make/D/N=2/O W_coef
	W_coef[0] = {1, angle}
	FuncFit/H="01"/NTHR=1 amplitude_fit W_coef rotation /X=error /D
	amplitude = W_coef[0]
	
	// Rescale the theory curves
	wave simple = $(theory_prefix + "Simple_population")
	wave nb1 = $(theory_prefix + "NB1_population")
	wave sk1 = $(theory_prefix + "SK1_population")
	wave task1e = $(theory_prefix + "TASK1e_population")
	wave task1t = $(theory_prefix + "TASK1t_population")
	
	make /o $(theory_prefix + "Simple_rescale") = amplitude * simple
	make /o $(theory_prefix + "NB1_rescale") = amplitude * nb1
	make /o $(theory_prefix + "SK1_rescale") = amplitude * sk1
	make /o $(theory_prefix + "TASK1e_rescale") = amplitude * task1e
	make /o $(theory_prefix + "TASK1t_rescale") = amplitude * task1t
end

function rescale_all()
	// macro to rescale all of the theory curves by an amplitude
	// extracted from data
	
	// 0.25 * pi
	rescale_theory_curves(0.25*pi, "root:Theory:X0_25_pi_", "root:Sequence_Data:piby4:")
	rescale_theory_curves(0.50*pi, "root:Theory:X0_50_pi_", "root:Sequence_Data:piby2:")
	rescale_theory_curves(0.75*pi, "root:Theory:X0_75_pi_", "root:Sequence_Data:pit3by4:")
	rescale_theory_curves(1.00*pi, "root:Theory:X1_00_pi_", "root:Sequence_Data:pi:")
	rescale_theory_curves(1.25*pi, "root:Theory:X1_25_pi_", "root:Sequence_Data:pit5by4:")
	rescale_theory_curves(1.50*pi, "root:Theory:X1_50_pi_", "root:Sequence_Data:pit3by2:")
	rescale_theory_curves(1.75*pi, "root:Theory:X1_75_pi_", "root:Sequence_Data:pit7by4:")
	rescale_theory_curves(2.00*pi, "root:Theory:X2_00_pi_", "root:Sequence_Data:pit2:")
end

function plot_all()
	// macro to plot all of the rescaled theory curves on top of data
	my_graph("root:Theory:X0_25_pi_", "root:Sequence_Data:piby4:")
	my_graph("root:Theory:X0_50_pi_", "root:Sequence_Data:piby2:")
	my_graph("root:Theory:X0_75_pi_", "root:Sequence_Data:pit3by4:")
	my_graph("root:Theory:X1_00_pi_", "root:Sequence_Data:pi:")
	my_graph("root:Theory:X1_25_pi_", "root:Sequence_Data:pit5by4:")
	my_graph("root:Theory:X1_50_pi_", "root:Sequence_Data:pit3by2:")
	my_graph("root:Theory:X1_75_pi_", "root:Sequence_Data:pit7by4:")
	my_graph("root:Theory:X2_00_pi_", "root:Sequence_Data:pit2:")
end

// datafolder:		subfolder in which existing data is stored
// newfolder: 		subfolder name in which to store new data waves
// datanames:	
function new_avg(datafolder, newfolder, datanames)
	string datafolder, newfolder, datanames

	
	variable nsequences = itemsinlist(datanames, ",")	
		
	string saveddatafldr = "root:Raw_Data:"+datafolder
	setdatafolder $saveddatafldr
	variable npts=numpnts(ScanPlot_x_avg_0)
	variable npasses=numpnts(ScanPlot_x_0)/npts
	
	// create new data folder to store result waves.  /s leaves us in this folder
	String newfldr = "root:Sequence_Data:" + newfolder
	NewDataFolder/o/s $newfldr
	
	// make shared x_axis data
	Duplicate/o $(saveddatafldr+":ScanPlot_x_avg_0"), x_error_params
	variable i
	for(i=0;i<nsequences; i+=1)
		string wvname=stringfromlist(i, datanames, ",")
		make/o/n=(npts) $wvname, $(wvname+"_error")
	endfor
	
	// cycle through raw data, transferring average data to named waves in new folder and creating
	// 	a new errorbar wave using the variance between scans instead of the spread in the histograms

	for(i=0; i<nsequences;i+=1)	// cycle through sequences
		wave data = $(saveddatafldr+":ScanPlot_y_"+num2str(i))
		wave newdata = $(newfldr+":"+stringfromlist(i, datanames, ","))
		wave newerror = $(newfldr+":"+stringfromlist(i, datanames, ",")+"_error")
		variable j,k
		
		for(j=0;j<npts;j+=1)	// cycle through points in one sequence
			variable ptavg=0
			variable var=0
			
			for(k=0; k<npasses;k+=1)		// sum over data for identical parameters
				ptavg=ptavg+data[k*npts+j]/npasses
				
			endfor
			for(k=0; k<npasses; k+=1)		
				var=var+(data[k*npts+j]-ptavg)^2
			endfor	
			newdata[j]=ptavg
			newerror[j]=sqrt(var/npasses)
			
		endfor
	
	endfor

Display rotation vs x_error_params
ErrorBars/T=2/L=2 rotation Y,wave=(rotation_error,rotation_error)
AppendToGraph SK1 vs x_error_params
ModifyGraph mode=4,rgb(SK1)=(0,0,65280)
ErrorBars/T=2/L=2 SK1 Y,wave=(SK1_error,SK1_error)
AppendToGraph NB1 vs x_error_params
ModifyGraph mode=0,rgb(NB1)=(0,26112,0)
ErrorBars/T=2/L=2 NB1 Y,wave=(NB1_error,NB1_error)
AppendToGraph TASK1e vs x_error_params
ModifyGraph rgb(TASK1e)=(0,0,0)
ErrorBars/T=2/L=2 TASK1e Y,wave=(TASK1e_error,TASK1e_error)
AppendToGraph TASK1t vs x_error_params
ModifyGraph rgb(TASK1t)=(26368,0,52224)
ErrorBars/T=2/L=2 TASK1t Y,wave=(TASK1t_error,TASK1t_error)
ModifyGraph lsize=2
Label left "D_5/2 Fraction";DelayUpdate
Label bottom "Rotation Error (epsilon)"
SetAxis left 0,*

Legend/C/N=text0/A=LT

SavePICT/EF=1/E=-8 as newfolder
	
end


function rescale(wvname, amplitude)
	string wvname
	variable amplitude
	wave wv = $wvname
	make /o $(wvname + "_rescale") = amplitude*wv
end

function rescale_beam(wvname)
	string wvname
	nvar Amp = root:Theory:Amp
	
	wave wv = $wvname
	make /o /n=(numpnts(wv)) $(wvname + "_rescale") = Amp * wv
end
	
function recenter(wvname)
	string wvname
	nvar Center = root:Theory:Center
	nvar Waist = root:Theory:Waist
	
	wave wv = $wvname
	make /o /n=(numpnts(wv)) $(wvname + "_recenter") = (wv - Center)/Waist
end

Function WaistFit(w,x) : FitFunc
	Wave w
	Variable x

	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(x) = 1/2 * (1 - Theta * cos( pi * exp( - (x-x0)^2 / w^2)))
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 3
	//CurveFitDialog/ w[0] = Theta
	//CurveFitDialog/ w[1] = w
	//CurveFitDialog/ w[2] = x0

	return 1/2 * (1 - w[0] * cos( pi * exp( - (x-w[2])^2 / w[1]^2)))
End

Function amplitude_fit(w,epsilon) : FitFunc
	Wave w
	Variable epsilon

	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(epsilon) = A /2 * (1 - cos(theta * (1 + epsilon)))
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ epsilon
	//CurveFitDialog/ Coefficients 2
	//CurveFitDialog/ w[0] = A
	//CurveFitDialog/ w[1] = theta

	return w[0] /2 * (1 - cos(w[1] * (1 + epsilon)))
End

function my_graph(theory_prefix, experiment_prefix)
	string theory_prefix
	string experiment_prefix
	//string data_folder = GetDataFolder(1)
	//SetDataFolder experiment_prefix
	
	// Build graph of experimental data and theory curves
	Display $(experiment_prefix + "rotation") vs $(experiment_prefix + "x_error_params")
	AppendToGraph $(theory_prefix + "Simple_rescale") vs $(theory_prefix + "Simple_epsilon")
	AppendToGraph $(experiment_prefix + "SK1") vs $(experiment_prefix + "x_error_params")
	AppendToGraph $(theory_prefix + "SK1_rescale") vs $(theory_prefix + "Simple_epsilon")
	AppendToGraph $(experiment_prefix + "NB1") vs $(experiment_prefix + "x_error_params")
	AppendToGraph $(theory_prefix + "NB1_rescale") vs $(theory_prefix + "Simple_epsilon")
	AppendToGraph $(experiment_prefix + "TASK1t") vs $(experiment_prefix + "x_error_params")
	AppendToGraph $(theory_prefix + "TASK1t_rescale") vs $(theory_prefix + "Simple_epsilon")
	AppendToGraph $(experiment_prefix + "TASK1e") vs $(experiment_prefix + "x_error_params")
	AppendToGraph $(theory_prefix + "TASK1e_rescale") vs $(theory_prefix + "Simple_epsilon")
	
	// Error bars
	ErrorBars rotation Y,wave=($(experiment_prefix + "rotation_error"),$(experiment_prefix + "rotation_error"))
	ErrorBars SK1 Y,wave=($(experiment_prefix + "SK1_error"),$(experiment_prefix + "SK1_error"))
	ErrorBars NB1 Y,wave=($(experiment_prefix + "NB1_error"),$(experiment_prefix + "NB1_error"))
	ErrorBars TASK1t Y,wave=($(experiment_prefix + "TASK1t_error"),$(experiment_prefix + "TASK1t_error"))
	ErrorBars TASK1e Y,wave=($(experiment_prefix + "TASK1e_error"),$(experiment_prefix + "TASK1e_error"))
	
	// Modify data markers
	ModifyGraph mode(rotation)=3,mode(SK1)=3,mode(NB1)=3,mode(TASK1e)=3,mode(TASK1t)=3
	ModifyGraph marker(rotation)=16,marker(SK1)=23,marker(NB1)=18,marker(TASK1e)=8,marker(TASK1t)=19
	ModifyGraph lSize(rotation)=2,lSize(SK1)=2,lSize(NB1)=2,lSize(TASK1e)=2,lSize(TASK1t)=2
	ModifyGraph rgb(rotation)=(0,0,0),rgb(SK1)=(65280,0,0),rgb(NB1)=(0,52224,0),rgb(TASK1e)=(0,0,65280)
	ModifyGraph rgb(TASK1t)=(16384,48896,65280)
	ModifyGraph msize(rotation)=2,msize(SK1)=2,msize(NB1)=2,msize(TASK1e)=2,msize(TASK1t)=2
	ModifyGraph mrkThick(rotation)=1,mrkThick(SK1)=1.5,mrkThick(TASK1e)=1
	ModifyGraph opaque(TASK1e)=1
	ModifyGraph useMrkStrokeRGB(TASK1e)=1
	ModifyGraph mrkStrokeRGB(SK1)=(65280,0,0),mrkStrokeRGB(TASK1e)=(0,0,65280)
	
	// Modify theory line style
	variable items = ItemsInList(theory_prefix, ":")
	string trace_name = StringFromList(items - 1, theory_prefix, ":")
	ModifyGraph lStyle($(trace_name + "NB1_rescale"))=9
	ModifyGraph lStyle($(trace_name + "SK1_rescale"))=3
	ModifyGraph lStyle($(trace_name + "TASK1t_rescale"))=1
	ModifyGraph rgb($(trace_name + "Simple_rescale"))=(0,0,0)
	ModifyGraph rgb($(trace_name + "NB1_rescale"))=(0,52224,0)
	ModifyGraph rgb($(trace_name + "SK1_rescale"))=(65280,0,0)
	ModifyGraph rgb($(trace_name + "TASK1e_rescale"))=(0,0,65280)
	ModifyGraph rgb($(trace_name + "TASK1t_rescale"))=(16384,48896,65280)
	
	// Set labels and axis scaling
	ModifyGraph tick=1
	ModifyGraph mirror=2
	ModifyGraph font(left)="Times New Roman"
	Label left "\\F'Times New Roman'\\S2\\MD\\B5/2\\M Population"
	Label bottom "\\F'Times New Roman'Normalized pulse area / \\F'Symbol'e\\F'Times New Roman'\\Bj"
	SetAxis left 0,1
	SetAxis bottom 0,1.2
end


Window Graph5() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Sequence_Data:pit5by4:
	Display /W=(195,176.75,589.5,385.25) rotation vs x_error_params
	AppendToGraph SK1 vs x_error_params
	AppendToGraph NB1 vs x_error_params
	AppendToGraph TASK1e vs x_error_params
	AppendToGraph TASK1t vs x_error_params
	AppendToGraph :::Theory:X1_25_pi_Simple_rescale[0,119] vs :::Theory:X1_25_pi_Simple_epsilon
	AppendToGraph :::Theory:X1_25_pi_NB1_rescale[0,119] vs :::Theory:X1_25_pi_NB1_epsilon
	AppendToGraph :::Theory:X1_25_pi_SK1_rescale[0,119] vs :::Theory:X1_25_pi_SK1_epsilon
	AppendToGraph :::Theory:X1_25_pi_TASK1e_rescale[0,119] vs :::Theory:X1_25_pi_TASK1e_epsilon
	AppendToGraph :::Theory:X1_25_pi_TASK1t_rescale[0,119] vs :::Theory:X1_25_pi_TASK1t_epsilon
	SetDataFolder fldrSav0
	ModifyGraph mode(rotation)=3,mode(SK1)=3,mode(NB1)=3,mode(TASK1e)=3,mode(TASK1t)=3
	ModifyGraph marker(rotation)=16,marker(SK1)=23,marker(NB1)=18,marker(TASK1e)=8,marker(TASK1t)=19
	ModifyGraph lSize(rotation)=2,lSize(SK1)=2,lSize(NB1)=2,lSize(TASK1e)=2,lSize(TASK1t)=2
	ModifyGraph lStyle(X1_25_pi_NB1_rescale)=9,lStyle(X1_25_pi_SK1_rescale)=3,lStyle(X1_25_pi_TASK1t_rescale)=1
	ModifyGraph rgb(rotation)=(0,0,0),rgb(SK1)=(65280,0,0),rgb(NB1)=(0,52224,0),rgb(TASK1e)=(0,0,65280)
	ModifyGraph rgb(TASK1t)=(16384,48896,65280),rgb(X1_25_pi_Simple_rescale)=(0,0,0)
	ModifyGraph rgb(X1_25_pi_NB1_rescale)=(0,52224,0),rgb(X1_25_pi_SK1_rescale)=(65280,0,0)
	ModifyGraph rgb(X1_25_pi_TASK1e_rescale)=(0,0,65280),rgb(X1_25_pi_TASK1t_rescale)=(16384,48896,65280)
	ModifyGraph msize(rotation)=2,msize(SK1)=2,msize(NB1)=2,msize(TASK1e)=2,msize(TASK1t)=2
	ModifyGraph mrkThick(rotation)=0.1,mrkThick(SK1)=1.5,mrkThick(TASK1e)=1
	ModifyGraph opaque(TASK1e)=1
	ModifyGraph useMrkStrokeRGB(TASK1e)=1
	ModifyGraph mrkStrokeRGB(SK1)=(65280,0,0),mrkStrokeRGB(TASK1e)=(0,0,65280)
	ModifyGraph tick=1
	ModifyGraph mirror=2
	ModifyGraph font(left)="Times New Roman"
	Label left "\\F'Times New Roman'\\S2\\MD\\B5/2\\M Population"
	Label bottom "\\F'Times New Roman'Normalized pulse area / \\F'Symbol'e\\F'Times New Roman'\\Bj"
	SetAxis left 0,1
	SetAxis bottom 0,1.2
	ErrorBars rotation Y,wave=(:Sequence_Data:pit5by4:rotation_error,:Sequence_Data:pit5by4:rotation_error)
	ErrorBars SK1 Y,wave=(:Sequence_Data:pit5by4:SK1_error,:Sequence_Data:pit5by4:SK1_error)
	ErrorBars NB1 Y,wave=(:Sequence_Data:pit5by4:NB1_error,:Sequence_Data:pit5by4:NB1_error)
	ErrorBars TASK1e Y,wave=(:Sequence_Data:pit5by4:TASK1e_error,:Sequence_Data:pit5by4:TASK1e_error)
	ErrorBars TASK1t Y,wave=(:Sequence_Data:pit5by4:TASK1t_error,:Sequence_Data:pit5by4:TASK1t_error)
EndMacro

Proc Graph5Style() : GraphStyle
	PauseUpdate; Silent 1		// modifying window...
	ModifyGraph/Z mode[0]=3,mode[1]=3,mode[2]=3,mode[3]=3,mode[4]=3
	ModifyGraph/Z marker[0]=16,marker[1]=23,marker[2]=18,marker[3]=8,marker[4]=19
	ModifyGraph/Z lSize[0]=2,lSize[1]=2,lSize[2]=2,lSize[3]=2,lSize[4]=2
	ModifyGraph/Z lStyle[6]=9,lStyle[7]=3,lStyle[9]=1
	ModifyGraph/Z rgb[0]=(0,0,0),rgb[1]=(65280,0,0),rgb[2]=(0,52224,0),rgb[3]=(0,0,65280)
	ModifyGraph/Z rgb[4]=(16384,48896,65280),rgb[5]=(0,0,0),rgb[6]=(0,52224,0),rgb[7]=(65280,0,0)
	ModifyGraph/Z rgb[8]=(0,0,65280),rgb[9]=(16384,48896,65280)
	ModifyGraph/Z msize[0]=2,msize[1]=2,msize[2]=2,msize[3]=2,msize[4]=2
	ModifyGraph/Z mrkThick[0]=0.1,mrkThick[1]=1.5,mrkThick[3]=1
	ModifyGraph/Z opaque[3]=1
	ModifyGraph/Z useMrkStrokeRGB[3]=1
	ModifyGraph/Z mrkStrokeRGB[1]=(65280,0,0),mrkStrokeRGB[3]=(0,0,65280)
	ModifyGraph/Z tick=1
	ModifyGraph/Z mirror=2
	ModifyGraph/Z font(left)="Times New Roman"
	Label/Z left "\\F'Times New Roman'\\S2\\MD\\B5/2\\M Population"
	Label/Z bottom "\\F'Times New Roman'Normalized pulse area / \\F'Symbol'e\\F'Times New Roman'\\Bj"
	SetAxis/Z left 0,1
	SetAxis/Z bottom 0,1.2
EndMacro
