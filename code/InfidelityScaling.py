#!/usr/bin/python
#
# Copyright (C) 2014 True Merrill
# Georgia Tech Research Institute
# Advanced Concepts Laboratory
#
#


from numpy import pi
from scipy.linalg import expm2 as expm
from pylab import *
from TASK1Pulses import *
import copy

#
# Methods to convert between (upsilon, alpha) parameterization and the
# (lambdaX, lambdaY) parameterization
#
def CalcLambdaX(upsilon, alpha):
    return upsilon / (2*pi)
    
def CalcLambdaY(upsilon, alpha):
    return alpha/pi * sqrt(1./3 - upsilon**2/(12 * alpha**2))
    
def CalcUpsilon(lambdaX, lambdaY):
    return 2*pi * lambdaX
    
def CalcAlpha(lambdaX, lambdaY):
    return pi * sqrt(lambdaX**2 + 3 * lambdaY**2)

def SK1(theta, phi):
    phiSK1 = arccos(-theta/(4*pi))
    sequence = Sequence([
        [theta, phi],
        [2*pi, phiSK1],
        [2*pi, -phiSK1]
        ])
    return sequence

def N2(theta, phi):
    phiN2 = arccos(-theta/(4*pi))
    sequence = Sequence([
        [theta, phi],
        [pi, phiN2],
        [2*pi, -phiN2],
        [pi, phiN2]
        ])
    return sequence

def FrequencyShift(theta, phi, gradient, epsilon):
    """Returns a propagator"""
    distance = sqrt(-log(epsilon))
    Hz=distance*gradient*Z
    H = epsilon*(X*cos(phi) + Y*sin(phi)) + Hz
    U = numpy.dot(expm(1j*theta*Hz/2.),expm(-1j * theta * H / 2.))
    return matrix(U)

def Infidelity(A, B):
    [q,Q] = numpy.linalg.eig(A.H * B)
    f = numpy.real( 1.j * numpy.log(q) )
    f = f - numpy.mean(f)
    
    # Remove over rotations
    #for index in range(len(f)):
    #    if abs( f[index] ) > pi/2.0:
    #        f[index] = f[index]%(pi/2.0)
        
    dst = 2.*max( numpy.sin(f/2.) )
    infd = dst**2 / 2.
    if infd > 1:
        infd = 2. - infd
    return infd

def ScaleSequence(sequence, epsilon):
    copySequence = copy.deepcopy(sequence)
    
    # For each pulse in the sequence, rescale the rotation angle argument
    for pulse in copySequence:
        pulse[0] = pulse[0] * epsilon
    return copySequence

def ScalingFunc(sequence, targetGate, epsilon):
    sequence = ScaleSequence(sequence, epsilon)
    propagator = sequence.Operator()
    return Infidelity(targetGate, propagator)

# Sequences to test.  
Sequences = [
    Sequence([[pi, 0]]),
    SK1(pi, 0),
    MinErrorTASK1(pi),
    N2(pi, 0),
    MinTimeTAN2(pi)
]



SequencesPlotStyle = [
    'k',
    'b',
    'r',
    'c',
    'g'
]

SequencesLabels = [
    'Simple',
    'SK1',
    'TASK1',
    'N2',
    'TAN2'
]

TargetGates = [
    R(0, 0), 
    R(pi, 0)
]


# This is a list of functions that accept a single argument,
# corresponding to the addressing error, and return a single float,
# corresponding to the gate infidelity
z = linspace(0, 2.5, 500)
epsilon = exp(-z**2)

fig = figure()
subplots = len(TargetGates)
ylabels = [r"$I$ Infidelity", r"$X$ Infidelity"]

for j in range(len(TargetGates)):
    
    # Create a argument for the subplot
    subplotArg = str(subplots) + "1" + str(j+1)
    subplotArg = int(subplotArg)
    sub = subplot(subplotArg)
    
    ylim((1e-5, 1))
    ylabel(ylabels[j])
    #sub.yaxis.set_ticks([1, 1e-2, 1e-4, 1e-6, 1e-8, 1e-10])

    # Add data for each of the pulse sequences
    for i in range(len(Sequences)):
	f = lambda epsilon: ScalingFunc(Sequences[i], TargetGates[j], epsilon)
        vf = vectorize(f)
        results = vf(epsilon)
        plotStyle = SequencesPlotStyle[i]
        semilogy(z, results, plotStyle, label = SequencesLabels[i])

    # Add data for the gradient method
    f = lambda epsilon: Infidelity(FrequencyShift(pi, 0, 2.*(1.-0.0995)/1.5840, epsilon), TargetGates[j])
    vf = vectorize(f)
    results = vf(epsilon)
    semilogy(z, results, "k-.", label = "Gradient")

xlabel(r"$z/w_0$")
legend()
show()
