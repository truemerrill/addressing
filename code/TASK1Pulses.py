# TASK1Pulses.py
#
# Methods to study the TASK1 sequences
#
# Copyright (C) 2012, 2013 True Merrill
# Georgia Institute of Technology
# School of Chemistry and Biochemistry
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cmath
import numpy
import scipy
import scipy.linalg
import scipy.optimize


# Create local copies of several numpy objects.
from numpy import exp, sqrt, sin, cos, tan, arcsin, arccos, arctan, \
     arctan2, mod, pi


# Pauli Matrices
I = numpy.matrix("1,0;0,1")
X = numpy.matrix("0,1;1,0")
Y = numpy.matrix("0,-1j;1j,0")
Z = numpy.matrix("1,0;0,-1")

# Define rotations
def R(theta, phi):
    """

    """
    if theta < 0:
        theta = -theta
        phi = phi + pi

    Sigma = cos(phi) * X + sin(phi) * Y
    U = cos(theta/2.) * I - 1.j * sin(theta/2.) * Sigma
    return U


# Sequence class
class Sequence(numpy.ndarray):
    """

    """
    def __new__(cls, input_array):
        obj = numpy.asarray(input_array).view(cls)
        return obj

    def __array_finalize__(self, obj):
        return None

    def Clean(self):
        """

        """
        for pulse in self:
            theta = pulse[0]
            phi = pulse[1]

            # Check for negative rotation angles.  If angles are
            # negative, apply a positive rotation about the opposite.
            if theta < 0:
                theta = -theta
                phi = phi + pi

            # Check that phi is in (0, 2pi)
            phi = mod(phi, 2*pi)

            # Update stored values
            pulse[0] = theta
            pulse[1] = phi

    def Operator(self):
        """

        """
        U = I
        for pulse in self:
            U = R(pulse[0], pulse[1]) * U

        return U

    def Arg(self):
        """

        """
        Unitary = self.Operator()

        # Diagonalize Unitary
        (u, V) = scipy.linalg.eig(Unitary)
        V = numpy.matrix( V )
        L = V.H * Unitary * V

        # Calculate Arg for each eigenvalue
        A = 0 * L
        for index in range( len(L) ):
            A[index, index] = cmath.phase( L[index,index] )

        return V * A * V.H

    def Decomp(self):
        """

        """
        A = self.Arg()
        vec = numpy.real([
            -numpy.trace( A * X ),
            -numpy.trace( A * Y ),
            -numpy.trace( A * Z )
            ])
        return vec

    def Time(self):
        """

        """
        time = 0
        for pulse in self:
            time = time + abs( pulse[0] )
        return numpy.array( float(time) )

    def Angle(self):
        """

        """
        U = self.Operator()
        Theta = numpy.real(2 * arccos(numpy.trace(I * U)/2))
        return Theta
    
    def String(self, disp = True):
        """
        
        """
        self.Clean()
        Table = ""
        for pulse in self:
            Table = Table + '%.5f\t%.5f\n' %(pulse[0], pulse[1])
        return Table

    def Table(self, disp = True):
        """
        Print (angles,phases) as a row in a pretty LaTeX table
        """
        table = ""
        
        # Add entry for theta and time
        string = '%.3f & %.3f & ' %(self.Angle(), self.Time())
        table = table + string

        # Add N-1 entries to table, seperating by LaTeX column marker &
        NumPulses = len(self)
        for index in range(NumPulses - 1):
            string = '(%.3f, %.3f) & ' %(self[index][0], self[index][1])
            table = table + string
        
        # Add final entry
        string = '(%.3f, %.3f) \\ \n' %(self[NumPulses-1][0], self[NumPulses-1][1])
        table = table + string
        
        if disp:
            print table
        
        return table


def ASK1(upsilon, alpha):
    """

    """
    phiASK1 = arccos( - upsilon / (2.0*alpha) )
    sequence = Sequence([
        [upsilon, 0],
        [alpha, phiASK1],
        [alpha, -phiASK1]
        ])
    
    # Add Upsilon and Alpha to sequence
    setattr(sequence, "Upsilon", upsilon)
    setattr(sequence, "Alpha", alpha)

    # Add an error method to ASK1 sequences
    Error = numpy.array([ alpha**4 / 32 * ( sin(2*phiASK1)**2 )])
    setattr(sequence, "Error", Error)
    
    return sequence


def TASK1(upsilon, alpha):
    """

    """
    # Compute ASK1 and decompose into a rotation axis
    A = ASK1(upsilon, alpha)
    [ax, ay, az] = A.Decomp()

    # Measure angles
    Beta = arctan2( az, sqrt(ax**2 + ay**2) )
    phiBeta = arctan2( ay, ax )

    # Add toggling rotations to ASK1 sequence
    sequence = Sequence([
        [Beta, 3*pi/2.],
        [A[0][0], A[0][1] - phiBeta],
        [A[1][0], A[1][1] - phiBeta],
        [A[2][0], A[2][1] - phiBeta],
        [Beta, pi/2.]
        ])
    
    # Add Upsilon and Alpha to sequence
    setattr(sequence, "Upsilon", upsilon)
    setattr(sequence, "Alpha", alpha)
    
    # Add an error method to TASK1 sequences
    Error = A.Error
    setattr(sequence, "Error", Error)
    
    return sequence


def MinTimeASK1(theta, disp = True):
    """
    Minimize the sequence time using Sequential Least Squares Programming
    """
    
    def time(x):
        """Time function for the ASK1 sequence"""
        A = ASK1( x[0], x[1] )
        return A.Time()

    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = ASK1( x[0], x[1] )
        return numpy.array([ theta - A.Angle() ])
    
    def inequality_constraint(x):
        """
        A function 'f' such that f(x) >= 0 in a succesful constrained
        optimization problem.  We use this here to ensure that 2 alpha
        > upsilon.
        """
        return numpy.array([2*x[1] - x[0]])
    
    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 2*pi),
               (0, 2*pi) ]
    
    # Initial guess for the solution (in [upsilon, alpha])
    if theta < 2*pi:
        alpha_guess = 2.*arccos(1. - theta/pi)
        x0 = numpy.array([alpha_guess, alpha_guess])
    else:
        x0 = numpy.array([pi/2.0, pi/2.0])

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        time, x0, 
        f_eqcons = equality_constraint,
        f_ieqcons = inequality_constraint, 
        bounds = bounds,
        disp = disp
        )
    
    # Return the optimal ASK1 sequence
    sequence = ASK1( optimal[0], optimal[1] )
    
    return sequence


def MinErrorASK1(theta, disp = True):
    """
    Minimize the sequence time using Sequential Least Squares Programming
    """
    
    def error(x):
        """
        Error function for the ASK1 sequence.  This is an analytical
        expression for the leading-order infidelity of the operation
        applied to the neighboring ion.
        """
        phiASK1 = arccos( - x[0] / (2*x[1]) )
        return numpy.array([ 
                x[1]**4 / 32 * ( sin(2*phiASK1)**2 )
                ])

    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = ASK1( x[0], x[1] )
        return numpy.array([ theta - A.Angle() ])
    
    def inequality_constraint(x):
        """
        A function 'f' such that f(x) >= 0 in a succesful constrained
        optimization problem.  We use this here to ensure that 2 alpha
        > upsilon.
        """
        return numpy.array([2*x[1] - x[0]])
    
    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 2*pi),
               (0, 2*pi) ]
    
    # Initial guess for the solution (in [upsilon, alpha])
    if theta < 2*pi:
        alpha_guess = 2.*arccos(1. - theta/pi)
        x0 = numpy.array([alpha_guess, alpha_guess])
    else:
        x0 = numpy.array([pi/2.0, pi/2.0])

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        error, x0, 
        f_eqcons = equality_constraint,
        f_ieqcons = inequality_constraint, 
        bounds = bounds,
        disp = disp
        )
    
    # Return the optimal ASK1 sequence
    sequence = ASK1( optimal[0], optimal[1] )

    return sequence


def MinTimeTASK1(theta, disp = True):
    """
    Minimize the sequence time using Sequential Least Squares Programming
    """
    
    def time(x):
        """Time function for the TASK1 sequence"""
        A = TASK1( x[0], x[1] )
        return A.Time()

    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = TASK1( x[0], x[1] )
        return numpy.array([ theta - A.Angle() ])
    
    def inequality_constraint(x):
        """
        A function 'f' such that f(x) >= 0 in a succesful constrained
        optimization problem.  We use this here to ensure that 2 alpha
        > upsilon.
        """
        return numpy.array([2*x[1] - x[0]])
    
    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 2*pi),
               (0, 2*pi) ]
    
    # Initial guess for the solution (in [upsilon, alpha])
    if theta < 2*pi:
        alpha_guess = 2.*arccos(1. - theta/pi)
        x0 = numpy.array([alpha_guess, alpha_guess])
    else:
        x0 = numpy.array([pi/2.0, pi/2.0])

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        time, x0, 
        f_eqcons = equality_constraint,
        f_ieqcons = inequality_constraint, 
        bounds = bounds,
        disp = disp
        )
    
    # Return the optimal TASK1 sequence
    sequence = TASK1( optimal[0], optimal[1] )
    
    return sequence


def MinErrorTASK1(theta, disp = True):
    """
    Minimize the sequence time using Sequential Least Squares Programming
    """
    
    def error(x):
        """
        Error function for the ASK1 sequence.  This is an analytical
        expression for the leading-order infidelity of the operation
        applied to the neighboring ion.
        """
        phiASK1 = arccos( - x[0] / (2*x[1]) )
        return numpy.array([ 
                x[1]**4 / 32 * ( sin(2*phiASK1)**2 )
                ])


    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = TASK1( x[0], x[1] )
        return numpy.array([ theta - A.Angle() ])
    
    def inequality_constraint(x):
        """
        A function 'f' such that f(x) >= 0 in a succesful constrained
        optimization problem.  We use this here to ensure that 2 alpha
        > upsilon.
        """
        return numpy.array([2*x[1] - x[0]])
    
    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 2*pi),
               (0, 2*pi) ]
    
    # Initial guess for the solution (in [upsilon, alpha])
    if theta < 2*pi:
        alpha_guess = 2.*arccos(1. - theta/pi)
        x0 = numpy.array([alpha_guess, alpha_guess])
    else:
        x0 = numpy.array([pi/2.0, pi/2.0])
        
    if theta < pi/4:
        acc = 1e-8
    else:
        acc = 1e-6

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        error, x0, 
        f_eqcons = equality_constraint,
        f_ieqcons = inequality_constraint, 
        bounds = bounds,
        disp = disp,
        acc = acc
        )
    
    # Return the optimal TASK1 sequence
    sequence = TASK1( optimal[0], optimal[1] )

    return sequence


def AN2(upsilon, alpha):
    """
    
    """
    phiAN2 = arccos( - upsilon / (4.0*alpha) )
    sequence = Sequence([
        [upsilon, 0],
        [alpha, phiAN2],
        [2*alpha, -phiAN2],
        [alpha, phiAN2]
        ])

    # Add Upsilon and Alpha to sequence
    setattr(sequence, "Upsilon", upsilon)
    setattr(sequence, "Alpha", alpha)

    # Add an error method to AN2 sequences
    Error = numpy.array([
        (alpha**6 / 72.0) * (5 + 4*cos(2*phiAN2)) * (sin(2*phiAN2)**2)
        ])
    setattr(sequence, "Error", Error)

    return sequence


def TAN2(upsilon, alpha):
    """

    """
    # Compute AN2 and decompose into a rotation axis
    A = AN2(upsilon, alpha)
    [ax, ay, az] = A.Decomp()

    # Measure angles
    
    # Measure angles
    Beta = arctan2( az, sqrt(ax**2 + ay**2) )
    phiBeta = arctan2( ay, ax )

    # Add toggling rotations to AN2 sequence
    sequence = Sequence([
        [Beta, 3*pi/2.],
        [A[0][0], A[0][1] - phiBeta],
        [A[1][0], A[1][1] - phiBeta],
        [A[2][0], A[2][1] - phiBeta],
        [A[3][0], A[3][1] - phiBeta],
        [Beta, pi/2.]
        ])
    
    # Add Upsilon and Alpha to sequence
    setattr(sequence, "Upsilon", upsilon)
    setattr(sequence, "Alpha", alpha)
    
    # Add an error method to TAN2 sequences
    Error = A.Error
    setattr(sequence, "Error", Error)
    
    return sequence


def MinTimeTAN2(theta, disp = True):
    """
    Minimize the sequence time using Sequential Least Squares Programming
    """
    
    def time(x):
        """Time function for the TAN2 sequence"""
        A = TAN2( x[0], x[1] )
        return A.Time()

    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = TAN2( x[0], x[1] )
        return numpy.array([ theta - A.Angle() ])
    
    def inequality_constraint(x):
        """
        A function 'f' such that f(x) >= 0 in a succesful constrained
        optimization problem.  We use this here to ensure that 4 alpha
        > upsilon.
        """
        return numpy.array([4*x[1] - x[0]])
    
    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 3*pi),
               (0, pi) ]
    
    # Initial guess for the solution (in [upsilon, alpha])
    if theta < 2*pi:
        alpha_guess = theta #2.*arccos(1. - theta/pi)
        x0 = numpy.array([alpha_guess, alpha_guess/2.0])
    else:
        x0 = numpy.array([pi, pi/2.0])

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        time, x0, 
        f_eqcons = equality_constraint,
        f_ieqcons = inequality_constraint, 
        bounds = bounds,
        disp = disp
        )
    
    # Return the optimal TAN2 sequence
    sequence = TAN2( optimal[0], optimal[1] )
    
    return sequence


def MinErrorTAN2(theta, disp = True):
    """
    Minimize the sequence time using Sequential Least Squares Programming
    """
    
    def error(x):
        """
        Error function for the AN2 sequence.  This is an analytical
        expression for the leading-order infidelity of the operation
        applied to the neighboring ion.
        """
        phiAN2 = arccos( - x[0] / (4*x[1]) )
        prefactor = 1e3
        return numpy.array([ 
                1e3 * x[1]**6 /72.0 * (5 + 4*cos(2*phiAN2)) * (sin(2*phiAN2)**2)
                ])


    def equality_constraint(x):
        """
        A function 'f' such that f(x) == 0 in a succesful constrained
        optimization problem.  We use this here to ensure the sequence
        produces a net rotation by the angle theta.
        """
        A = TAN2( x[0], x[1] )
        return numpy.array([ theta - A.Angle() ])
    
    def inequality_constraint(x):
        """
        A function 'f' such that f(x) >= 0 in a succesful constrained
        optimization problem.  We use this here to ensure that 4 alpha
        > upsilon.
        """
        epsilon = 1e-4
        return numpy.array([4*x[1] - epsilon - x[0]])
    
    # A list of tuples specifying the lower and upper bound for each
    # independent variable.
    bounds = [ (0, 3*pi),
               (0, pi) ]
    
    # Initial guess for the solution (in [upsilon, alpha])
    if theta < 2*pi:
        alpha_guess = 2.75194*(0.15228 + sqrt(0.00437774 + 0.72676*theta)) #theta #2.*arccos(1. - theta/pi)
        x0 = numpy.array([alpha_guess, alpha_guess])
    else:
        x0 = numpy.array([pi, pi/2.0])
        
    if theta < pi/4:
        acc = 1e-8
    else:
        acc = 1e-6

    # Perform optimization
    optimal = scipy.optimize.fmin_slsqp(
        error, x0, 
        f_eqcons = equality_constraint,
        f_ieqcons = inequality_constraint, 
        bounds = bounds,
        disp = disp,
        acc = acc
        )
    
    # Return the optimal TAN2 sequence
    sequence = TAN2( optimal[0], optimal[1] )

    return sequence


# 
# MAIN PROGRAM
#
if __name__ == "__main__":
    
    angles = numpy.arange(pi/4, 2*pi, pi/8)
    angles[len(angles)-1] = 2*pi - 1e-4
    
    header = "TASK1Pulses.py\n\n" +\
        "Copyright (C) 2012, 2013 True Merrill\n" +\
        "School of Chemistry and Biochemistry\n" +\
        "Georgia Institute of Technology\n"
        
    
    print header
    print "1. Computing time-optimal ASK1 sequences"
    print "----------------------------------------"
    table = ""
    for angle in angles:
        sol = MinTimeASK1(angle, disp = False)
        table = table + sol.Table(disp = False)
        
    print "Printing table:\n\n"
    print table
    print "\n"
    
    print "2. Computing time-optimal TASK1 sequences"
    print "-----------------------------------------"
    table = ""
    for angle in angles:
        sol = MinTimeTASK1(angle, disp = False)
        table = table + sol.Table(disp = False)
        
    print "Printing table:\n\n"
    print table
    print "\n"
    
    print "3. Computing error-optimal ASK1 sequences"
    print "-----------------------------------------"
    table = ""
    for angle in angles:
        sol = MinErrorASK1(angle, disp = False)
        table = table + sol.Table(disp = False)
        
    print "Printing table:\n\n"
    print table
    print "\n"
    
    print "4. Computing error-optimal TASK1 sequences"
    print "------------------------------------------"
    table = ""
    for angle in angles:
        sol = MinErrorTASK1(angle, disp = False)
        table = table + sol.Table(disp = False)
        
    print "Printing table:\n\n"
    print table
    print "\n"
