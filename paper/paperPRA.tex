% ************************************
% Optimized composite-sequence ion addressing in a surface-electrode trap
%   J. True Merrill
%   S. Charles Doret
%   Grahame Vittorini
%   JP Addison
%   Kenneth Brown


%\documentclass[10pt,letterpaper,aps,prl,preprint]{revtex4-1}
\documentclass[10pt,aps,pra,twocolumn,showpacs,amsmath,superscriptaddress]{revtex4-1}

% ************************************
% Preamble
% ************************************
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{bm}
\usepackage{multirow}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}


% ************************************
% User specified commands for document
% ************************************
\renewcommand{\i}{\mathrm{i}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\tr}{\text{tr}}
\renewcommand{\vec}[1]{\bm{#1}}
\newcommand{\algb}[1]{\mathfrak{#1}}
\newcommand{\Id}{1}
\newcommand{\beamz}{\zeta}
\newif\iflong
\longtrue

% ************************************
% Front matter
% ************************************
\begin{document}
	\title{Transformed Composite Sequences for Improved Qubit Addressing}
	\author{J. True Merrill}
	\email{true.merrill@gtri.gatech.edu}
	\affiliation{Georgia Tech Research Institute, Atlanta, GA 30332}
	\affiliation{School of Chemistry and Biochemistry, Georgia Institute of Technology, Atlanta, GA 30332}
	\author{S. Charles Doret}
	\altaffiliation[Current address: ]{Department of Physics, Williams College, Williamstown, MA 01267}
	\affiliation{Georgia Tech Research Institute, Atlanta, GA 30332}
	\author{Grahame Vittorini}
	\altaffiliation[Current address: ]{Joint Quantum Institute and Department of Physics, University of Maryland, College Park, MD 20742}
	\affiliation{School of Physics, Georgia Institute of Technology, Atlanta, GA 30332}
	\author{J.P. Addison}
	\affiliation{School of Physics, Georgia Institute of Technology, Atlanta, GA 30332}
	\author{Kenneth R. Brown}
	\affiliation{School of Chemistry and Biochemistry, Georgia Institute of Technology, Atlanta, GA 30332}
	\affiliation{School of Physics, Georgia Institute of Technology, Atlanta, GA 30332}
	\affiliation{School of Computational Science and Engineering, Georgia Institute of Technology, Atlanta, GA 30332}
	\date{\today}

\begin{abstract}
Selective laser addressing of a single atom or atomic ion qubit can be improved using narrowband composite pulse sequences.  We describe a Lie-algebraic technique to generalize known narrowband sequences and introduce new sequences related by dilation and rotation of sequence generators. Our method improves known narrowband sequences by decreasing both the pulse time and the residual error.  Finally, we experimentally demonstrate these composite sequences using $^{40}$Ca$^+$ ions trapped in a surface-electrode ion trap.  
\end{abstract}

% 03.67.Lx -- Quantum computation
% 32.80.Qk -- Coherent control of atomic interactions with photons
% 37.10.Ty -- Ion traps

\pacs{03.67.Lx, 32.80.Qk, 37.10.Ty}
\keywords{composite pulse sequence, addressing errors, spatial resolution}
\maketitle


% ************************************
% Introduction
% ************************************
\section{Introduction}
In ion traps~\cite{Cirac1995, Nagerl1999} and neutral atom optical lattices~\cite{Scheunemann2000, Dumke2002}, single-qubit addressing typically requires focused lasers where the beam waist is smaller than the inter-atom separation.  Closely spaced atoms are generally desirable to improve two-qubit coupling rates, often demanding inter-atom spacings approaching the diffraction limit.  In practice, single-qubit addressing requires precise focal alignment and ultra-stable beam steering to prevent unwanted excitations on neighboring atoms, a significant challenge as the number of qubits increases~\cite{Knoernschild2010}.  Furthermore, achieving the tight focus required for single-ion addressing is often made difficult by geometric constraints and restricted optical access ~\cite{Seidelin2006, Doret2012}.  These factors combine to make single-qubit addressing a major challenge in many experimental systems.

One method of improving single-qubit addressing applies an auxiliary field gradient to shift qubit transition frequencies, affording a degree of selective control~\cite{Wang2009, Zhang2006,Kunert2014}.  Similar techniques use an intense laser to introduce a position-dependent AC Stark shift~\cite{Staanum2002,  Weitenberg2011} or a microwave gradient to impose an AC Zeeman shift~\cite{Warring2013}. Recently, quantum control has been used in conjunction with frequency shifts to achieve addressing with inhomogeneous control fields \cite{Jessen2013}. A recent proposal examined spatial refocusing through precise laser positioning coupled with controlled phase shifts \cite{ShenPRA2013}.  These methods require time-consuming calibrations to remove systematic errors while adding to experimental complexity.  These techniques also make strong physical assumptions of the nature of the qubit, and are generally not transferable to other physical qubits.  

In this article we demonstrate an alternative control scheme that replaces simple single-qubit gates with a narrowband composite sequence of laser pulses designed for local addressing~\cite{Ivanov2011, Merrill2013}.  These sequences allow the exclusive manipulation of a single qubit even when neighboring qubits are subjected to significant laser intensity, without required auxiliary fields.   Such compensating sequences reduce systematic control errors at the cost of increasing gate times~\cite{Merrill2013}.   
Our main result is a new technique to generate fully-compensating narrowband sequences using Lie-algebraic transformations of other known sequences.  We use numerical minimization to identify sequences with superior error correction properties and low operation times compared to the original sequence family.   Further, we demonstrate the effectiveness of these sequences for single-qubit addressing in an experiment with $^{40}$Ca$^+$ ion qubits in a surface-electrode trap.

% ************************************
%  Model, resonant pulses, addressing errors
% ************************************
\section{Theoretical Methods}
\subsection{Model Hamiltonian}
We consider a register of $N$ identical spatially separated qubits.  A resonant laser in the rotating-wave limit illuminates an addressed qubit $i$, but also illuminates neighboring qubits $j$ at a lower intensity ($j \neq i$), resulting in an addressing error that yields a separable but correlated evolution on each qubit.  Control over the qubits is implemented by applying a time-dependent Hamiltonian
\begin{align}
H(t) = \frac{\hbar \Omega_i}{2} \left\{ \sigma_i(\varphi(t)) +\sum_ {j\neq i} \epsilon_j \sigma_j(\varphi(t)) \right\},
\label{eq:hamiltonian}
\end{align}
where $\varphi(t)$ is the laser phase, $\Omega_i$ is the Rabi frequency for the addressed qubit $i$, and $\sigma(\varphi(t)) = X \cos \varphi(t) + Y \sin \varphi(t)$, where $X$, $Y$ are Pauli operators.  For simplicity we fix $|\Omega_i|^2$ to some maximal bounded value corresponding to the intensity peak of the laser field.  The terms $\epsilon_j \sigma_j(\varphi(t))$ induce undesired correlated rotations on neighboring qubits.  Here the ratio $\epsilon_j = \Omega_j / \Omega_i < 1$, where $\Omega_j$ is the Rabi frequency at the neighboring qubit $j$.  The frequency $\Omega_j$ parameterizes the magnitude of the addressing error and is assumed to be fixed over the entire duration of the control.  The time dependence of $H(t)$ is entirely due to the temporal modulation of the phase $\varphi(t)$, which here serves as our only control parameter. %This choice confers no loss of generality, since solutions with a time-dependent laser intensity may be considered with an appropriate substitution of the time variable. 

\subsection{Narrowband sequences}
Compensating pulse sequences choose $\varphi(t)$ to yield a net evolution robust against a particular class of systematic errors.  A common simplification for $\varphi(t)$ is to divide the time coordinate into $L$ time intervals $(\Delta t_1, \Delta t_2, \dots, \Delta t_L)$ for which the phase is a constant angle $(\varphi_1, \varphi_2, \dots, \varphi_L)$.  Each pulse applies a spin rotation controlled by the generator of rotations $r_\ell = -\i \theta_\ell \sigma(\varphi_\ell) / 2$, where $\theta_\ell = \Omega_i \Delta t_\ell$ is the pulse area or rotation angle applied to the addressed qubit.  The total propagator for the entire sequence is $U(\vec{r}) = U_i(\vec{r}) \left[ \bigotimes_{j \neq i} U_j(\vec{r}) \right]$ where 
\begin{align}
U_i(\vec{r}) = \prod_{\ell=1}^L \exp ( r_\ell ), \qquad U_j(\vec{r}) = \prod_{\ell=1}^L \exp ( \epsilon_j r_\ell )
\end{align}
are the gates applied to the addressed qubit $i$ and the neighbor qubit $j$ respectively, and $\vec{r} = (r_{1}, r_{2}, \dots, r_{L})$ is the ordered set of rotation generators.  Following the usual convention, multiplication of successive pulse propagators occurs from the left to ensure the correct time-ordering.  With a careful choice of rotation generators, it is possible to produce propagators that apply a nontrivial gate $U_i(\vec{r}) = U_T$ to the addressed qubit while simultaneously approximating the identity $U_j(\vec{r}) = I + O(\epsilon_j^{n + 1})$ on all neighboring qubits.  Sequences with this property are called $n$th-order fully-compensating narrowband sequences \cite{Brown2004, Ivanov2011, Merrill2013, Low2013}.  So long as $\Omega_j \ll \Omega_i$ these sequences reduce errors on neighboring qubits even if the laser simultaneously illuminates several qubits.

% ************************************
% Sequence algebra and mapping technique
% ************************************

We remark that an $n$th-order narrowband sequence must satisfy a set of $n$ Lie-algebraic constraints on the rotation generators.  Applying the Baker-Campbell-Hausdorff lemma we find that $U_j(\vec{r}) = \exp \left \{ \sum_{m = 1}^{\infty} \epsilon^m_j {F}_m (\vec{r}) \right \}$, where $F_m (\vec{r})$ is given by the generators and their commutators and is related to the $m$th-order average Hamiltonian.  Explicitly the first two terms are 
\begin{align}
	F_1(\vec{r}) = \sum_{\ell = 1}^{L} r_{\ell}, \qquad
	F_2(\vec{r}) = \frac{1}{2} \sum_{\ell = 1}^{L}\sum_{k = 1}^{\ell} [ r_{\ell}, r_{k} ]. 
\end{align}
To satisfy $U_j = I + O(\epsilon_j^{n+1})$ for all values of $\epsilon_j$, each $F_m(\vec{r})$ with $m \leq n$ must independently equal zero.  Frequently it is possible to assign a geometric interpretation to each constraint.  For example, $F_1(\vec{r}) = 0$ requires the generators $\vec{r}$ to form a closed figure on the Lie algebra, and $F_2(\vec{r}) = 0$ requires that the figure encloses signed areas of equal magnitude but opposite sign.  

\begin{figure}
  \begin{center}
    \includegraphics{figures/pdf/figure1}
  \end{center}
  \caption{(Color online) Construction of $\vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y)$ by compositions of dilation and rotation maps applied to $\vec{r}_{\text{SK1}}(2\pi)$. The maps ensure $F_1(\vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y)) = 0$ and therefore $\vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y)$ generates a first-order narrowband sequence.  Particular choices of $(\lambda_x, \lambda_y, R)$ result in different net rotations, total sequence pulse areas, and residual sequence errors. 
  }
    \label{figure1}
\end{figure}

\subsection{Transformed sequences}
We introduce a method to generalize existing narrowband sequences by identifying Lie-algebraic transformations on the generators which leave the constraint equations satisfied.  These transformations yield derivative sequences which achieve the same order of error suppression, but may offer substantial improvements in the total composite gate time as well as the gate accuracy.  Our method can be described as follows.  Let $\mathscr{T}_\lambda : \algb{su}(2) \mapsto \algb{su}(2)$ be a map between Lie algebra elements with the condition that if $F_m(\vec{r}) = 0$, then $F_m ( \mathscr{T}_\lambda \circ \vec{r}) = 0$  for all $m \leq n$.  This ensures that if $\vec{r}$ generates an $n$th-order compensating sequence, then $\vec{r}(\lambda) = \mathscr{T}_\lambda \circ \vec{r}$ also generates a sequence of the same order, however in general $U_i(\vec{r}(\lambda)) \neq U_i(\vec{r})$.  To find sequences that implement a particular target gate $U_T$, we perform an optimization over the mapped sequences to minimize a cost functional while constraining $U_i(\vec{r}(\lambda)) = U_T$.  Two cost functionals we consider are the total pulse area $\theta_\text{Total} = \sum_\ell^L |\theta_\ell|$, related to the total time required to perform a composite gate, and the infidelity of the effective identity gate on the neighboring qubits $\mathcal{I}(I) = 1 - \mathcal{F}(I, U_j(\vec{r}(\lambda)))$, where $\mathcal{F}(V, U) = |\text{tr}[V^\dagger U ]/2|$ is the fidelity between gates $V$ and $U$.

Maps that satisfy the constraint condition are common affine transformations. For arbitrary sequences, compositions of rotations and dilations fulfill the requirement: $F_m(R \vec{r} R^\dagger) = R F_m(\vec{r}) R^\dagger$ and $F_m(\lambda\vec{r}) = \lambda^m F_m(\vec{r})$. Independent dilation of each axis $\mathscr{T}_{\lambda_x} \circ X = \lambda_x X$ and $\mathscr{T}_{\lambda_y} \circ Y = \lambda_y Y$ will also satisfy this criteria for $n \leq 2$, and in our case, where the controls are restricted to the $X$-$Y$ plane, for $n \leq 3$.  Starting with an initial seed sequence $\vec{r}$ we generate a family of related sequences $\vec{r}(\lambda_x, \lambda_y, R)$ by the composition of dilations and rotations (see Fig.~\ref{figure1}).  

\subsection{Transforming SK1 to TASK1}
As an example, consider the first-order passband SK1 pulse sequences, produced by the generators $\vec{r}_\text{SK1}(\theta)  = (-\i \theta \sigma(0)/2 , -\i \pi \sigma(\varphi_\text{SK1}), -\i \pi \sigma(-\varphi_\text{SK1}))$ where $\cos \varphi_\text{SK1} =\theta/(4\pi)$~\cite{Brown2004}.  On the addressed qubit SK1 applies $U_i(\vec{r}_{\text{SK1}}(\theta)) = \exp(- \i \theta X / 2)$ and it approximates the identity on neighboring qubits, $U_j(\vec{r}_{\text{SK1}}(\theta))=I+O(\epsilon_j^2)$.  To illustrate our transformation method, we start with the specific case $\vec{r}_{\mathrm{SK1}}(2\pi)$ and identify a map which recovers the entire SK1 family.  Let $\mathscr{T}_\theta$ be the one parameter map that contracts the $X$ components by $\lambda_x=\theta/(2\pi)$ and expands the $Y$ components by $\lambda_y=\sqrt{(4-\lambda_x^2)/3}$.  This map satisfies $F_1(\mathscr{T}_\theta \circ \vec{r}_{\mathrm{SK1}}(2\pi)) = 0$ and $\vec{r}_{\mathrm{SK1}}(\theta) = \mathscr{T}_\theta \circ \vec{r}_{\mathrm{SK1}}(2\pi)$.   

SK1 can implement an arbitrary single-qubit gate using extra rotations,  $U_T = R U_i(\vec{r}_{\text{SK1}}(\theta)) R^\dagger$.  Alternatively, one simply changes the sequence generators using the similarity transformation $\vec{r}_{\text{SK1}} (\theta, R) = R \vec{r}_{\text{SK1}}(\theta) R^\dagger$. For a target in-plane rotation $U_T=\exp(- \i \theta \sigma(\varphi_T) / 2)$ this can be achieved by advancing all the $\varphi_l$ in SK1 by $\varphi_T$.

The composition of independent $X$ and $Y$ dilation maps applied to $\vec{r}_{\mathrm{SK1}}(2\pi)$ generates a larger family of narrowband sequences that we call ASK1, $\vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y) = \mathscr{T}_{\lambda_y} \circ \mathscr{T}_{\lambda_x} \circ \vec{r}_\text{SK1} (2 \pi)$.  Fig.~\ref{figure1} illustrates the construction of ASK1 sequences.   Note that ASK1 usually applies a net rotation $U_i(\vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y))$ about an axis outside of the $X$-$Y$ plane; such a sequence cannot replace an in-plane rotation implemented by a single resonant pulse with a constant phase.   To achieve a target in-plane gate $U_T$ using the Hamiltonian \eqref{eq:hamiltonian} and phase advances, we introduce the similarity transformation $\vec{r}_{\text{ASK1}} (\lambda_x, \lambda_y, R) = R \vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y) R^\dagger$, where $U_T = U_i(\vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y, R))$.  We decompose $R = \exp( r' ) T$, where $\exp( r' )$ applies the minimum-angle rotation to match the polar angle of the rotation axis; $T$ is a rotation about $Z$ which can be implemented by a uniform phase advance on the inner ASK1 pulses.  The phase advance controls the azimuthal angle of the net rotation axis.  This transformed sequence construction, which we call TASK1, sets the net rotation angle with the innermost pulses.  In terms of rotation generators the sequence is $\vec{r}_{\text{TASK1}}(\lambda_x, \lambda_y, R) = (r', T \vec{r}_{\text{ASK1}}(\lambda_x, \lambda_y) T^\dagger, -r')$.     


\begin{figure}
  \begin{center}
    \includegraphics{figures/pdf/figure2}
  \end{center}
  \caption{ (Color online) (a) The narrowband TASK1 family in terms of the scale parameters $(\lambda_x,\lambda_y)$.  The TASK1 sequences implement $U_T = \exp(-\i \theta X / 2)$ on addressed qubits, $\theta$ is the net rotation angle.  Contours of $\theta$ (solid) and $\theta_{\text{Total}}$ (dashed) are plotted in intervals of $\pi/4$.  The SK1 sequences (blue, thick solid) are a subfamily of TASK1. (b) TASK1~($T_\text{min}$) (red, dashed) is the subfamily which minimizes $\theta_\text{Total}$.  (c) TASK1~($E_\text{min}$) (green, solid) minimizes $\mathcal{I}(I)$.  %The time-minimal and error-minimal subfamilies outperform SK1 in both total pulse area and gate infidelity. 
  }
  \label{figure2}
\end{figure}

% ************************************
% Optimality of TASK1
% ************************************
Despite the inclusion of two additional pulses, the TASK1 sequences outperform SK1 in both the total pulse area and the infidelity.  
 Fig. \ref{figure2}a shows the TASK1 family in terms of $(\lambda_x, \lambda_y)$ and plots contours of the net rotation angle and total pulse area.  %SK1 sequences are denoted by the one parameter family with $\lambda_y = \sqrt{(4-\lambda_x^2)/3}$.  
 Using constrained optimization we identify a subfamily of sequences that minimize the total pulse area, TASK1~($T_\text{min}$), and the infidelity, TASK1~($E_\text{min}$) for a fixed net rotation angle.  We find the error-minimal sequences correspond to $\lambda_x = \lambda_y$ and result in ASK1 sequences homologous to equilateral triangles in  $\algb{su}(2)$. Fig. \ref{figure2}b compares the infidelity and total pulse area for each sequence subfamily.  We see that TASK1~($T_\text{min}$) and TASK1~($E_\text{min}$) outperform SK1 in both the required time and the minimization of the residual rotation on the neighboring qubit.  We also note that the time minimal and error minimal sequences yield similar performance for most net rotation angles.  In particular, for a target rotation $U_T = \exp \left( -\i \pi X / 2 \right)$, the error-minimal and time-minimal sequences are identical, $\lambda_x = \lambda_y = 1/2$, and TASK1 performs the gate using $3/5$ of the total pulse-area and with $1/5$ of the residual infidelity compared to SK1.  Explicit descriptions of the pulses can be found in the Appendix.


% ************************************
% Experimental demonstration
% ************************************
\section{Experimental demonstration}
\iflong %comment out by logic
\begin{figure}
  \begin{center}
    \includegraphics[width=\columnwidth]{figures/pdf/figure3}
  \end{center}
  \caption{(Color online) Population inversion as a function of normalized pulse area $\epsilon_j$ for target gates $U_T$ composed of simple rotations or by SK1, TASK1~($T_\text{min}$), or TASK1~($E_\text{min}$) sequences.  In (a) $U_T = -\i X$ and in (b) $U_T = \exp(-\i \pi X/4)$.  Curves are theoretical predictions with a single, common adjustable parameter accounting for experimental qubit detection fidelity.  %Narrowband sequences suppress population inversion for small $\epsilon_j$ but perform $U_T$ when $\epsilon_j = 1$. 
%series of compensating pulse sequences. Solid lines are theoretical predictions with a single, 
    \label{figure3}
  }
\end{figure}
\fi

We demonstrate these sequences by addressing individual $^{40}\text{Ca}^+$ ions confined in a microfabricated surface-electrode trap~\cite{Doret2012}; details of our surface trap setup can be found in~\cite{Vittorini2013}.  We use a 397~nm laser to Doppler cool and optically pump ions into the $|1\rangle = {^2}\text{S}_{1/2}\:(m_j = -1/2)$ state.  A narrow linewidth ($\gamma \sim 150$~Hz) 729~nm laser tuned to the $|1\rangle \rightarrow |0\rangle = {^2}\text{D}_{5/2}\:(m_j = -5/2)$ qubit transition is used to sideband cool the ion to $\leq 0.1$~phonons in all motional modes and to perform subsequent qubit rotations.  The 729~nm beam propagates parallel to the trap surface, intersecting the trap symmetry plane at a $45^\circ$ angle with a $w_0 = 44.2 \pm 0.8$~$\mu$m 1/$\e^2$ diameter waist.  Fast laser switching and phase control is achieved using an acousto-optic modulator driven by an amplified 16-bit direct-digital synthesizer with 20~ns timing resolution.  After applying a sequence of laser pulses, we measure the $|1\rangle$ state population using resonant ion fluorescence induced by the Doppler cooling laser.   

\iflong
We verify our theoretical predictions for TASK1 sequences by measuring the qubit state-transfer for differing pulse areas, controlled by adjusting the timings of each laser pulse to scale the energy-time product by a constant multiple $\epsilon_j$.  The resulting propagation is thus equivalent to the evolution that would be experienced during an addressing error by neighboring ions over differing laser intensities.  Fig.~\ref{figure3}a and Fig.~\ref{figure3}b compare the measured response for pulse sequences applying $U_T = -\i X$ and $U_T = \exp(-\i \pi X / 4)$ respectively to a target ion.  We observe that unwanted population inversion is suppressed when $\epsilon_j < 1$, as desired.  When $\epsilon_j \simeq 1$, corresponding to the pulse area experienced by the target ion, the observed state transfer is consistent with the expected gate.  We find excellent agreement between the calculated and measured response as a function of pulse area.
\fi

\begin{figure}
  \begin{center}
    \includegraphics[width=\columnwidth]{figures/pdf/figure4}
  \end{center}
  \caption{(Color online) Population inversion as a function of ion position for target $U_T = -\i X$ gates composed of simple rotations or TASK1 family pulses. The simple rotation maps out the intensity profile of the laser beam along the trap axis.  Narrowband sequences allow for an effective reduction in beam waist without changing the experimental setup.  Curves are theoretical predictions with a single, common adjustable parameter accounting for experimental qubit detection fidelity.
    \label{figure4}
  }
\end{figure}

We observe addressing error compensation by measuring the state transfer as a function of ion position relative to the center of the addressing beam (Fig.~\ref{figure4}).  The ion position along the trap axis is controlled to better than $\pm 0.5$~$\mu$m by biasing a subset of 46 segmented DC trap electrodes~\cite{Doret2012}.  We find that TASK1 sequences exhibit predicted narrowband behaviour: unwanted rotations on ions far from the beam are suppressed, while at the beam center the desired rotation is executed.  SK1 sequences are passband~\cite{Brown2004} and both reduces the sensitivity of the target gate to position uncertainty and reduces the unwanted rotation on the neighboring qubit~\cite{Merrill2013}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\columnwidth]{figures/pdf/figure5}
  \end{center}
  \caption{(Color online) Gate infidelity as a function of ion position compared to (a) the identity operation and (b) $U_T = -\i X$ gates composed of a simple rotation, TASK1 family pulses, or a simple rotation with a qubit frequency gradient $\Delta/z \geq 1.137$~$\Omega/w_0$.  The gradient is chosen so that at $z/w_0 = 1.584$ both TASK1~($T_\text{min}$) and the gradient technique perform an identity gate with infidelity $\mathcal{I}(I) = 10^{-4}$.  Compared to gradient techniques narrowband sequences are less susceptible to ion positioning errors.
    \label{figure5}
  }
\end{figure}

\section{Effect of ion position and beam fluctuation}
In order to quantify the gain of these techniques in terms of ion spacing, we calculate the fidelity of the applied gate with respect to both the $I$ gate (Fig. \ref{figure5}a) and the $-\i X$ gate (Fig. \ref{figure5}b) as a function of position from the center of a Gaussian beam with waist $w_0$ . %SK1 as a passband sequence both reduces the sensitivity of the target gate to position uncertainty and reduces the unwanted rotation on the neighboring qubit~\cite{Brown2004}.  TASK1 is a narrowband sequence and improves ion discrimination at the cost of a greater sensitivity to beam position.
For a target infidelity on the neighboring ion $\mathcal{I}(I) \leq 10^{-4}$, we find the ion separation must be greater than 2.172~$w_0$ for the simple pulse, 1.704~$w_0$ for SK1, and 1.584~$w_0$ for TASK1~($T_\text{min}$).  

We also compare pulse sequence techniques to addressing methods that uses auxiliary field gradients~\cite{Wang2009, Zhang2006,Kunert2014}.  To facilitate comparison between the methods, we require a gradient as large as $\Delta/z \geq 1.137$~$\Omega/w_0$, where $\Delta$ is the applied shift in the qubit resonance frequency.  At this strength, the gradient method produces an identity gate with infidelity $\mathcal{I}(I) \leq 10^{-4}$ on the neighboring qubits while simultaneously performing $-\i X$ on the addressed qubit with the same ion separation and gate time as TASK1~($T_\text{min}$).  For our setup, where the beam waist is intentionally large to reduce the fractional positioning error in $z/w_0$, this corresponds to a modest magnetic field gradient of 90 mT/m.  However with diffraction limited optical beams and $\Omega = 1$~MHz Rabi frequency the required gradient is as large as 111~T/m.  As a point of comparison, strong gradients of 20 T/m have been achieved with permanent magnets \cite{Khromova2013} and on-chip currents have yielded static gradients of 0.06 T/m  \cite{Kunert2014} and microwave gradients of 35 T/m \cite{Warring2013}.  Gates performed using the gradient technique are also more sensitive to ion positioning errors.  To apply a $-\i X$ gate to the target ion with infidelity $\mathcal{I}(-\i X) \leq 10^{-4}$ requires positioning the ion to $z/w_0 \leq 0.0071$ for the gradient technique but for TASK1~($T_\text{min}$) the ion positioning tolerance is $z/w_0 \leq 0.072$.

Throughout our analysis we have assumed the laser beam position is fixed.  Fluctuations in laser beam position lead to errors on the addressed qubit, where the amplitude depends quadratically on the beam displacement $\beamz$, and reduce the error cancellation properties of the composite pulse on the neighboring qubits, where the amplitude has a linear dependence on $\beamz$. For slow fluctuations relative to the pulse time, the cancellation properties of composite pulses are retained \cite{KabytayevPRA2014}. For an adversarial noise that switches the laser position in time with the pulses, the infidelity of the addressed qubit scales as $(\beamz /w_0)^4$ and the infidelity of the neighboring qubit at position $z$ scales as $(\epsilon z\beamz /w_0^2)^2$. The adversarial model leads to the condition that in order to achieve an infidelity dominated by the residual term of the composite pulse, $\propto\epsilon^4$, then $\beamz/w_0 < \epsilon w_0/z$. To achieve equivalent fidelities on the addressed and neighboring qubit, requires that $\beamz$ be more tightly controlled than the case of addressing a single ion by a factor $w_0/z$.  This is only a fractional change in the pointing stability but can be important in the case of tightly focused beams.  We emphasize this is a worse case scenario and that for position instabilities uncorrelated with the pulse sequence that this error will be significantly averaged away.

% ************************************
% Conclusions
% ************************************
\section{Conclusion}

In conclusion, we introduced a Lie-algebraic transform method to produce narrowband sequences from other known sequences.%, frequently improving total pulse area and error suppression.  
Using the technique, we developed the TASK1 family from transformations of SK1 and demonstrated their suitability for single-qubit addressing in an experiment with trapped ions. The TASK1 family resultes in improved total pulse area and error suppression.  Our transformation method is particularly well suited for narrowband sequences, where there is no desired unitary operation on the neighboring qubits.  Application of the mapping technique to other narrowband sequences, e.g. NB1 and the N2$j$ family \cite{Wimperis1994, Brown2004}, is straightforward.  Applying the technique to sequences where the errors occur on the addressed qubit, e.g. detuning and amplitude errors, should also be possible, however, there is additional complexity due to the control rotating the errors to a toggled frame \cite{Merrill2013}. %The complication is that the scaling of the controls not only scales the error, but also changes the toggled frame; 
In these cases Lie-algebraic maps cannot consist only of dilations but must also account for the frame transformation. %Equilateral broadband sequences of $\pi$ pulses~\cite{Jones2013} should be a promising seed sequence for this approach. 
Further, it is possible to concatenate these pulses with additional sequences that correct detuning errors.  These concatenated sequences should also assist with addressability concerns in systems with variable splitting frequencies, such as superconducting qubits~\cite{GambettaPRL2012}.


\begin{acknowledgments}
The authors thank J. M. Amini, A. M. Meier, and Kenton R. Brown for helpful discussions and critical review of the manuscript. GV and SCD thank GTRI Shackelford Fellowships for graduate and postdoctoral support, respectively. This work was supported by the Office of the Director of National Intelligence - Intelligence Advanced Research Projects Activity through Army Research Office contract W911NF-10-1-0231 and Department of Interior contract D11PC20167. 
\end{acknowledgments}


\appendix
\section{Appendix: Explicit pulse sequences}
\label{app}
The main text describes rotation generators in terms of coordinates of vectors on a Lie algebra.  Here to aid in experimental implementation, we report in Table \ref{tab:pulses} each pulse in terms of a rotation angle and phase, $r_\ell = - \i \theta_\ell \sigma(\varphi_\ell) / 2$.  The reported sequences each generate a net rotation about the $X$ axis.  To shift the net rotation axis by an azimuthal angle $\varphi_T$, one adds $\varphi_T$ to the phase of each individual pulse in the sequence.  
 

\begin{table*}
\caption{Pulse sequences for TASK1 ($T_\text{min}$) and TASK1 ($E_\text{min}$). The rotation angle and phase for each pulse $r_\ell = - \i \theta_\ell \sigma(\varphi_\ell) / 2$ is  listed as well as the $\lambda_x$ and $\lambda_y$ used to scale the ASK1 sequence from the SK1 sequence. The last two columns show the total pulse area and the leading term of the infidelity. 
}\label{tab:pulses}
\begin{tabular*}{\textwidth}{c c c c c c c c c c c }
\hline \\[-1.0 em]
\hline \\[-0.7 em]
\multirow{2}{*}{Sequence}& \multirow{2}{*}{Net rotation} & \multirow{2}{*}{$\lambda_x$} & \multirow{2}{*}{$\lambda_y$} & $\theta_1$ & $\theta_2$ & $\theta_3$ & $\theta_4$ & $\theta_5$ & \multirow{2}{*}{Pulse area} & \multirow{2}{*}{Infidelity/$\epsilon_j^4$}\\
[-0.25 em] &  &  &  & $\varphi_1$ & $\varphi_2$ & $\varphi_3$ & $\varphi_4$ & $\varphi_5$ &  &  \\
\\[-0.7 em] \hline \\[-0.5 em]
\multirow{2}{*}{TASK1 ($T_\text{min}$)} & \multirow{2}{*}{$\pi/4$} & \multirow{2}{*}{0.2730} & \multirow{2}{*}{0.1828} & 0.6817 & 1.7155 & 1.3133 & 1.3133 & 0.6817 & \multirow{2}{*}{5.7055} & \multirow{2}{*}{0.0910} \\
[-0.1 em] &  &  &  & 1.5708 & 1.2177 & 3.5002 & 5.2184 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$\pi/2$} & \multirow{2}{*}{0.3988} & \multirow{2}{*}{0.2723} & 0.3013 & 2.5057 & 1.9404 & 1.9404 & 0.3013 & \multirow{2}{*}{6.9890} & \multirow{2}{*}{0.4308} \\
[-0.1 em] &  &  &  & 1.5708 & 1.2348 & 3.5075 & 5.2453 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$3\pi/4$} & \multirow{2}{*}{0.5000} & \multirow{2}{*}{0.3550} & 0.0000 & 3.1416 & 2.4898 & 2.4898 & 0.0000 & \multirow{2}{*}{8.1213} & \multirow{2}{*}{1.1510} \\
[-0.1 em] &  &  &  & 1.5708 & 1.2566 & 3.5101 & 5.2863 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$\pi$} & \multirow{2}{*}{0.5000} & \multirow{2}{*}{0.5000} & 0.0000 & 3.1416 & 3.1416 & 3.1416 & 0.0000 & \multirow{2}{*}{9.4248} & \multirow{2}{*}{2.2830} \\
[-0.1 em] &  &  &  & 1.5708 & 1.0472 & 3.1416 & 5.2360 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$5\pi/4$} & \multirow{2}{*}{0.5532} & \multirow{2}{*}{0.6227} & 0.1309 & 3.4758 & 3.8081 & 3.8081 & 0.1309 & \multirow{2}{*}{11.3539} & \multirow{2}{*}{4.3347} \\
[-0.1 em] &  &  &  & 4.7124 & 0.8958 & 2.9405 & 5.1343 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$3\pi/2$} & \multirow{2}{*}{0.6447} & \multirow{2}{*}{0.7135} & 0.3447 & 4.0507 & 4.3792 & 4.3792 & 0.3447 & \multirow{2}{*}{13.4984} & \multirow{2}{*}{7.7300} \\
[-0.1 em] &  &  &  & 4.7124 & 0.8251 & 2.8767 & 5.0567 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$7\pi/4$} & \multirow{2}{*}{0.7578} & \multirow{2}{*}{0.8246} & 0.5262 & 4.7613 & 5.0795 & 5.0795 & 0.5262 & \multirow{2}{*}{15.9728} & \multirow{2}{*}{14.2640} \\
[-0.1 em] &  &  &  & 4.7124 & 0.5860 & 2.6446 & 4.8106 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$2\pi$} & \multirow{2}{*}{1.0000} & \multirow{2}{*}{1.0000} & 0.0000 & 6.2832 & 6.2832 & 6.2832 & 0.0000 & \multirow{2}{*}{18.8496} & \multirow{2}{*}{36.5284} \\
[-0.1 em] &  &  &  & 4.7124 & 1.0472 & 3.1416 & 5.2360 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{TASK1 ($E_\text{min}$)} & \multirow{2}{*}{$\pi/4$} & \multirow{2}{*}{0.2226} & \multirow{2}{*}{0.2226} & 0.8001 & 1.3984 & 1.3984 & 1.3984 & 0.8001 & \multirow{2}{*}{5.7953} & \multirow{2}{*}{0.0896} \\
[-0.1 em] &  &  &  & 1.5708 & 1.0472 & 3.1416 & 5.2360 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$\pi/2$} & \multirow{2}{*}{0.3268} & \multirow{2}{*}{0.3268} & 0.4826 & 2.0534 & 2.0534 & 2.0534 & 0.4826 & \multirow{2}{*}{7.1255} & \multirow{2}{*}{0.4167} \\
[-0.1 em] &  &  &  & 1.5708 & 1.0472 & 3.1416 & 5.2360 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$3\pi/4$} & \multirow{2}{*}{0.4159} & \multirow{2}{*}{0.4159} & 0.2301 & 2.6134 & 2.6134 & 2.6134 & 0.2301 & \multirow{2}{*}{8.3002} & \multirow{2}{*}{1.0932} \\
[-0.1 em] &  &  &  & 1.5708 & 1.0472 & 3.1416 & 5.2360 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$\pi$} & \multirow{2}{*}{0.5000} & \multirow{2}{*}{0.5000} & 0.0000 & 3.1416 & 3.1416 & 3.1416 & 0.0000 & \multirow{2}{*}{9.4248} & \multirow{2}{*}{2.2830} \\
[-0.1 em] &  &  &  & 1.5708 & 1.0472 & 3.1416 & 5.2360 & 4.7124 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$5\pi/4$} & \multirow{2}{*}{0.5841} & \multirow{2}{*}{0.5841} & 0.2301 & 3.6698 & 3.6698 & 3.6698 & 0.2301 & \multirow{2}{*}{11.4696} & \multirow{2}{*}{4.2510} \\
[-0.1 em] &  &  &  & 4.7124 & 1.0472 & 3.1416 & 5.2360 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$3\pi/2$} & \multirow{2}{*}{0.6732} & \multirow{2}{*}{0.6732} & 0.4826 & 4.2298 & 4.2298 & 4.2298 & 0.4826 & \multirow{2}{*}{13.6545} & \multirow{2}{*}{7.5020} \\
[-0.1 em] &  &  &  & 4.7124 & 1.0472 & 3.1416 & 5.2360 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$7\pi/4$} & \multirow{2}{*}{0.7774} & \multirow{2}{*}{0.7774} & 0.8001 & 4.8848 & 4.8848 & 4.8848 & 0.8001 & \multirow{2}{*}{16.2547} & \multirow{2}{*}{13.3445} \\
[-0.1 em] &  &  &  & 4.7124 & 1.0472 & 3.1416 & 5.2360 & 1.5708 &  &  \\
[0.25 em]\multirow{2}{*}{} & \multirow{2}{*}{$2\pi$} & \multirow{2}{*}{1.0000} & \multirow{2}{*}{1.0000} & 0.0000 & 6.2832 & 6.2832 & 6.2832 & 0.0000 & \multirow{2}{*}{18.8496} & \multirow{2}{*}{36.5284} \\
[-0.1 em] &  &  &  & 4.7124 & 1.0472 & 3.1416 & 5.2360 & 1.5708 &  &  \\
[0.25 em]\hline
\end{tabular*}

\end{table*}

\bibliographystyle{linkapsrev}
\bibliography{paper}

\end{document}
